from copy import deepcopy
from typing import Any, List, Literal, Optional
import discord
from discord.ext import commands
from discord import app_commands
import logging
import os
from GamesManifest import GamesManifest
from Ruleset import BvBRuleset, NUM_GAMES
from Scoreboard import BvBScoreboard

SCOREBOARDS_DIR = 'scoreboards'
RULESETS_DIR = 'rulesets'

GAMES_JSON = 'games.json'

g_Scoreboard: BvBScoreboard = None
g_Manifest:   GamesManifest = None

g_GuestID:    int  = -1
g_GamerID:    int  = -1
g_AdminID:    int  = -1
g_ModID:      int  = -1
g_Names: List[str] = []

name_ids = Literal[1, 2]
choice_ids = Literal[1, 2, 3, 4, 5, 6, 7, 8, 9]
game_ids = Literal[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]

def run_by_approved_member():
    def predicate(interaction: discord.Interaction) -> bool:
        return (interaction.guild.get_role(g_GamerID) in interaction.user.roles) or (interaction.guild.get_role(g_GuestID) in interaction.user.roles)
    return app_commands.check(predicate)

def run_by_moderator_or_admin():
    def predicate(interaction: discord.Interaction) -> bool:
        return (interaction.guild.get_role(g_ModID) in interaction.user.roles) or (interaction.guild.get_role(g_AdminID) in interaction.user.roles)
    return app_commands.check(predicate)
    
def run_by_admin():
    def predicate(interaction: discord.Interaction) -> bool:
        return (interaction.guild.get_role(g_AdminID) in interaction.user.roles)
    return app_commands.check(predicate)

class ScoreSheet(app_commands.Group):
    def __init__(self, bot: commands.Bot, *args, **kwargs):
        self.bot = bot
        
        super(ScoreSheet, self).__init__(*args, **kwargs)
    
    async def on_error(self, interaction: discord.Interaction, command: app_commands.Command[Any, Any, Any], error: app_commands.AppCommandError) -> None:
        if type(error) is app_commands.errors.CheckFailure:
            if command.parent.parent is None:
                logging.log(logging.WARNING, f'{interaction.user.name} attempted to execute "/score {command.name}" without permissions.')
                await interaction.response.send_message(f'You do not have permission to execute "/score {command.name}".', ephemeral=True)
            else:
                logging.log(logging.WARNING, f'{interaction.user.name} attempted to execute "/score {command.parent.name} {command.name}" without permissions.')
                await interaction.response.send_message(f'You do not have permission to execute "/score {command.parent.name} {command.name}".', ephemeral=True)
        else:
            await interaction.response.send_message(f'Unknown error occurred - {error}.')
        return None
    
    @app_commands.command(name='test')
    async def _test(self, interaction: discord.Interaction):
        print(interaction.guild.roles)
    
    # ------------------------------------------------------------------------------------------------------------------------------
    # /score ban <game>
    # Toggles the ban flag of a game in the open scoreboard
    #-------------------------------------------------------------------------------------------------------------------------------
    @app_commands.command(name='ban')
    @app_commands.describe(gameid='Game to ban')
    @run_by_moderator_or_admin()
    async def _ban(self, interaction: discord.Interaction, gameid: int):
        await do_ban(interaction, gameid)
    
    # Autocomplete the game selection by taking all games
    @_ban.autocomplete('gameid')
    async def _ban_ac(self, interaction: discord.Interaction, current: str) -> List[app_commands.Choice[int]]:
        global g_Scoreboard
        return await g_Scoreboard.get_games_can_pick(current)
    
    
    # ------------------------------------------------------------------------------------------------------------------------------
    # /score close <save>
    # Closes the open scoreboard, with or without saving
    #-------------------------------------------------------------------------------------------------------------------------------
    @app_commands.command(name='close')
    @app_commands.describe(save='Save file before closing?')
    @run_by_moderator_or_admin()
    async def _close(self, interaction: discord.Interaction, save: bool):
        await interaction.response.defer(ephemeral=True, thinking=True)
        global g_Scoreboard
        try:
            if save:
                await g_Scoreboard.export_json()
                g_Scoreboard = None
                await interaction.followup.send(f'Saved and closed!', ephemeral=True)
            else:
                g_Scoreboard = None
                await interaction.followup.send(f"Closed without saving!", ephemeral=True)
        except Exception as e:
            logging.log(logging.WARNING, f'Failed to close file. {e.with_traceback()}.')
            await interaction.followup.send(f'Failed to close file. {e}.', ephemeral=True)
    
    
    # ------------------------------------------------------------------------------------------------------------------------------
    # /score dump <filename>
    # Dumps all of the intermediate images in given scoreboard
    #-------------------------------------------------------------------------------------------------------------------------------
    @app_commands.command(name='dump')
    @app_commands.describe(filename='File to dump images from')
    @run_by_moderator_or_admin()
    async def _dump(self, interaction: discord.Interaction, filename: str):
        await interaction.response.defer(ephemeral=True, thinking=True)
        try:
            # Create base copy of desired scoreboard, and a working copy
            old_scoreboard = BvBScoreboard(g_Manifest)
            await old_scoreboard.import_json(f'{SCOREBOARDS_DIR}/{filename}')
            scoreboard = BvBScoreboard(g_Manifest)
            
            # Setup initial values for dumping and communicating
            file_prefix = filename.removesuffix('.json')
            total_img = 2*(old_scoreboard.num_picked + 1)
            wm: discord.WebhookMessage = await interaction.followup.send(f'Beginning processing of {total_img} files.', ephemeral=True)
            
            # Export base image without bans
            scoreboard.duo       = old_scoreboard.duo
            scoreboard.names     = deepcopy(old_scoreboard.names)
            scoreboard.games     = deepcopy(old_scoreboard.games)
            wm = await wm.edit(content=f'Processing image 1 out of {total_img}...\nCreating empty image...')
            await scoreboard.export_img(f'{file_prefix}_0P.png')
            
            # Export base image with bans
            scoreboard.bans = deepcopy(old_scoreboard.bans)
            wm = await wm.edit(content=f'Processing image 2 out of {total_img}...\nCreating ban image...')
            await scoreboard.export_img(f'{file_prefix}_0W.png')
            
            # For each pick:
            for i in range(old_scoreboard.num_picked):
                
                # Create the image with the new game picked
                if old_scoreboard.picks[i] == -1 or old_scoreboard.choosers == -1:
                    await wm.edit(content=f'Finished processing {total_img} files early!')
                    break
                else:
                    scoreboard.picks[i]    = old_scoreboard.picks[i]
                    scoreboard.choosers[i] = old_scoreboard.choosers[i]
                    scoreboard.num_picked += 1
                    wm = await wm.edit(content=f'Processing image {i*2 + 3} out of {total_img}...\nCreating pick image...')
                    await scoreboard.export_img(f'{file_prefix}_{i + 1}P.png')
                
                # Create the image with the winner applied to the new game
                g = old_scoreboard.picks[i]
                if old_scoreboard.winners[g] == -1:
                    await wm.edit(content=f'Finished processing {total_img} files early!')
                    break
                else:
                    scoreboard.winners[g] = old_scoreboard.winners[g]
                    wm = await wm.edit(content=f'Processing image {i*2 + 4} out of {total_img}...\nCreating win image...')
                    await scoreboard.export_img(f'{file_prefix}_{i + 1}W.png')
                
            await wm.edit(content=f'Finished processing {total_img} files!')
        except Exception as e:
            logging.log(logging.WARNING, f'Failed to dump files. {e.with_traceback()}.')
            await interaction.followup.send(f'Failed to dump files. {e}.', ephemeral=True)
    
    @_dump.autocomplete('filename')
    async def _dump_ac(self, interaction: discord.Interaction, current: str) -> List[app_commands.Choice[str]]:
        filenames = [f for f in os.listdir(SCOREBOARDS_DIR) if f.endswith('.json')]
        filenames.sort()
        return_obj = []
        for f in filenames:
            if len(return_obj) >= 25:
                break
            if current.lower() in f.lower():
                return_obj.append(app_commands.Choice(name=f.removesuffix('.json'), value=f))
        return return_obj
    
    # ------------------------------------------------------------------------------------------------------------------------------
    
    @app_commands.command(name='init')
    @app_commands.describe(round='Round of the competition', name1='Name of the first person', name2='Name of the second person')
    @run_by_moderator_or_admin()
    async def _init(self, interaction: discord.Interaction, round: game_ids, name1: str, name2: str):
        await interaction.response.defer(ephemeral=True, thinking=True)
        global g_Manifest
        try:
            filename = f'[R{round}] {name1} vs {name2}.json'
            if os.path.exists(f'{SCOREBOARDS_DIR}/{filename}'):
                raise RuntimeError('File to create already exists')
            
            # Create and export new scoreboard
            scoreboard = BvBScoreboard(g_Manifest)
            await scoreboard.set_filename(f'{SCOREBOARDS_DIR}/{filename}')
            await scoreboard.set_name(1, name1)
            await scoreboard.set_name(2, name2)
            await scoreboard.set_game(7, 'battleship')
            await scoreboard.set_game(8, 'connectfour')
            await scoreboard.set_game(9, 'speedrunners')
            await scoreboard.export_json(f'{SCOREBOARDS_DIR}/{filename}')
            
            embed = await scoreboard.get_view_embed()
            embed.set_footer(text=f'Created new file {filename}!')
            await interaction.followup.send(embed=embed, ephemeral=True)
        except Exception as e:
            logging.log(logging.WARNING, f'Failed to create {filename}. {e.with_traceback()}.')
            await interaction.followup.send(f'Failed to create {filename}. {e}.', ephemeral=True)
    
    @_init.autocomplete('name1')
    async def _init_name1_ac(self, interaction: discord.Interaction, current: str) -> List[app_commands.Choice[str]]:
        return await get_names_all(current)
    
    @_init.autocomplete('name2')
    async def _init_name2_ac(self, interaction: discord.Interaction, current: str) -> List[app_commands.Choice[str]]:
        return await get_names_all(current)
    
    # ------------------------------------------------------------------------------------------------------------------------------
    
    @app_commands.command(name='load')
    @app_commands.describe(filename='File to open')
    @run_by_moderator_or_admin()
    async def _load(self, interaction: discord.Interaction, filename: str):
        await interaction.response.defer(ephemeral=True, thinking=True)
        global g_Scoreboard
        try:
            if g_Scoreboard is not None:
                raise RuntimeError('A file is already open. Please close it before continuing')
            g_Scoreboard = BvBScoreboard(g_Manifest)
            await g_Scoreboard.import_json(f'{SCOREBOARDS_DIR}/{filename}')
            embed = await g_Scoreboard.get_view_embed()
            embed.set_footer(text=f'Opened {filename}!')
            await interaction.followup.send(embed=embed, ephemeral=True)
        except Exception as e:
            logging.log(logging.WARNING, f'Failed to extract and open {filename}. {e.with_traceback()}.')
            await interaction.followup.send(f'Failed to extract and open {filename}. {e}.', ephemeral=True)
    
    @_load.autocomplete('filename')
    async def _load_ac(self, interaction: discord.Interaction, current: str) -> List[app_commands.Choice[str]]:
        filenames = [f for f in os.listdir(SCOREBOARDS_DIR) if f.endswith('.json')]
        filenames.sort()
        return_obj = []
        for f in filenames:
            if len(return_obj) >= 25:
                break
            if current.lower() in f.lower():
                return_obj.append(app_commands.Choice(name=f.removesuffix('.json'), value=f))
        return return_obj
    
    # ------------------------------------------------------------------------------------------------------------------------------
    
    @app_commands.command(name='pick')
    @app_commands.describe(nameid='Person picking the game', gameid='Game to pick')
    @run_by_moderator_or_admin()
    async def _pick(self, interaction: discord.Interaction, nameid: int, gameid: int):
        await interaction.response.defer(ephemeral=True, thinking=True)
        global g_Scoreboard
        try:
            if g_Scoreboard is None:
                await interaction.followup.send(f'There is no open file.', ephemeral=True)
                return
            current_picked = await g_Scoreboard.get_num_picked()
            await g_Scoreboard.set_pick   (current_picked + 1, gameid)
            await g_Scoreboard.set_chooser(current_picked + 1, nameid)
            game_name = await g_Scoreboard.get_game_display(gameid)
            name      = await g_Scoreboard.get_name(nameid)
            embed     = await g_Scoreboard.get_view_embed()
            embed.set_footer(text=f'{name} picked {game_name} as game {current_picked + 1}.')
            await interaction.followup.send(embed=embed, ephemeral=True)
        except Exception as e:
            logging.log(logging.WARNING, f'Failed to pick game. {e.with_traceback()}.')
            await interaction.followup.send(f'Failed to pick game. {e}.', ephemeral=True)
    
    @_pick.autocomplete('gameid')
    async def _pick_game_ac(self, interaction: discord.Interaction, current: str) -> List[app_commands.Choice[int]]:
        global g_Scoreboard
        return await g_Scoreboard.get_games_can_pick(current)
    
    @_pick.autocomplete('nameid')
    async def _pick_name_ac(self, interaction: discord.Interaction, current: str) -> List[app_commands.Choice[int]]:
        global g_Scoreboard
        return await g_Scoreboard.get_names(current)
    
    # ------------------------------------------------------------------------------------------------------------------------------
    
    @app_commands.command(name='process')
    @app_commands.describe(filename='File to create image of')
    @run_by_moderator_or_admin()
    async def _process(self, interaction: discord.Interaction, filename: str, reprocess: bool = True):
        await interaction.response.defer(ephemeral=True, thinking=True)
        try:
            if filename.endswith('/*'):
                directory = filename.removesuffix("/*")
                filenames = [
                    f for f in os.listdir(directory)
                    if f.endswith('.json')
                    and (reprocess or ((not reprocess) and (not os.path.exists(f"{f'{directory}/{f}'.removesuffix('.json')}.png"))))
                ]
                wm: discord.WebhookMessage = await interaction.followup.send(f'Beginning processing of {len(filenames)} files.', ephemeral=True)
                if directory == SCOREBOARDS_DIR:
                    for i in range(len(filenames)):
                        scoreboard = BvBScoreboard(g_Manifest)
                        filepath = f'{directory}/{filenames[i]}'
                        wm = await wm.edit(content=f'Processing {filenames[i]} - {i + 1} out of {len(filenames)}...')
                        await scoreboard.import_json(filepath)
                        await scoreboard.export_img()
                elif directory == RULESETS_DIR:
                    for i in range(len(filenames)):
                        ruleset = BvBRuleset(g_Manifest)
                        filepath = f'{directory}/{filenames[i]}'
                        wm = await wm.edit(content=f'Processing {filenames[i]} - {i + 1} out of {len(filenames)}...')
                        await ruleset.import_json(filepath)
                        await ruleset.export_img()
                await wm.edit(content=f'Finished processing {len(filenames)} files!')
            else:
                directory = filename.partition('/')[0]
                if directory == SCOREBOARDS_DIR:
                    scoreboard = BvBScoreboard(g_Manifest)
                    await scoreboard.import_json(filename)
                    await scoreboard.export_img()
                elif directory == RULESETS_DIR:
                    ruleset = BvBRuleset(g_Manifest)
                    await ruleset.import_json(filename)
                    await ruleset.export_img()
                
                await interaction.followup.send(file=discord.File(fp=f'{filename.removesuffix(".json")}.png'))
        except Exception as e:
            logging.log(logging.WARNING, f'Failed to process image. {e.with_traceback()}.')
            await interaction.followup.send(f'Failed to process image. {e}.', ephemeral=True)
    
    @_process.autocomplete('filename')
    async def _process_ac(self, interaction: discord.Interaction, current: str) -> List[app_commands.Choice[str]]:
        filenames = [f'{SCOREBOARDS_DIR}/{f}' for f in os.listdir(SCOREBOARDS_DIR) if f.endswith('.json')]
        filenames.extend([f'{RULESETS_DIR}/{f}' for f in os.listdir(RULESETS_DIR) if f.endswith('.json')])
        filenames.sort()
        return_obj = []
        for f in filenames:
            if len(return_obj) >= 23:
                break
            if current.lower() in f.lower():
                return_obj.append(app_commands.Choice(name=f.removesuffix('.json'), value=f))
        if current.lower() in f'{SCOREBOARDS_DIR}/*':
            return_obj.insert(0, app_commands.Choice(name=f'{SCOREBOARDS_DIR}/*', value=f'{SCOREBOARDS_DIR}/*'))
        if current.lower() in f'{RULESETS_DIR}/*':
            return_obj.insert(0, app_commands.Choice(name=f'{RULESETS_DIR}/*', value=f'{RULESETS_DIR}/*'))
        return return_obj
    
    # ------------------------------------------------------------------------------------------------------------------------------
    
    @app_commands.command(name='publish')
    @run_by_moderator_or_admin()
    async def _publish(self, interaction: discord.Interaction):
        await interaction.response.defer(ephemeral=True, thinking=True)
        global g_Scoreboard
        try:
            if g_Scoreboard is None:
                await interaction.followup.send(f'There is no open file to publish.', ephemeral=True)
                return
            filename = await g_Scoreboard.get_filename()
            await g_Scoreboard.export_img()
            await interaction.channel.send(file=discord.File(fp=f'{filename.removesuffix(".json")}.png'))
            await interaction.followup.send(f'Published image!', ephemeral=True)
        except Exception as e:
            logging.log(logging.WARNING, f'Failed to publish image. {e.with_traceback()}.')
            await interaction.followup.send(f'Failed to publish image. {e}.', ephemeral=True)
    
    # ------------------------------------------------------------------------------------------------------------------------------
    
    @app_commands.command(name='recache')
    @run_by_moderator_or_admin()
    async def _recache(self, interaction: discord.Interaction):
        await interaction.response.defer(ephemeral=True, thinking=True)
        global g_Manifest
        try:
            await g_Manifest.recalculate()
            await interaction.followup.send(f'Recalculated game cache!', ephemeral=True)
        except Exception as e:
            logging.log(logging.WARNING, f'Failed to recalculate game cache. {e.with_traceback()}.')
            await interaction.followup.send(f'Failed to recalculate game cache. {e}.', ephemeral=True)
        
    # ------------------------------------------------------------------------------------------------------------------------------
    
    @app_commands.command(name='send')
    @run_by_approved_member()
    async def _send(self, interaction: discord.Interaction, image: str):
        await interaction.response.defer(ephemeral=True, thinking=True)
        try:
            if os.path.exists(image):
                await interaction.channel.send(file=discord.File(fp=image))
            else:
                raise RuntimeError(f'Image does not exist at {image}')
            await interaction.followup.send(f'Sent image!', ephemeral=True)
        except Exception as e:
            logging.log(logging.WARNING, f'Failed to send image. {e.with_traceback()}.')
            await interaction.followup.send(f'Failed to send image. {e}.', ephemeral=True)
    
    @_send.autocomplete('image')
    async def _send_ac(self, interaction: discord.Interaction, current: str) -> List[app_commands.Choice[str]]:
        filenames = [f'{SCOREBOARDS_DIR}/{f}' for f in os.listdir(SCOREBOARDS_DIR) if f.endswith('.png')]
        filenames.extend([f'{RULESETS_DIR}/{f}' for f in os.listdir(RULESETS_DIR) if f.endswith('.png')])
        filenames.sort()
        return_obj = []
        for f in filenames:
            if len(return_obj) >= 25:
                break
            if current.lower() in f.lower():
                return_obj.append(app_commands.Choice(name=f.removesuffix('.png'), value=f))
        return return_obj
    
    # ------------------------------------------------------------------------------------------------------------------------------
    
    @app_commands.command(name='save')
    @run_by_moderator_or_admin()
    async def _save(self, interaction: discord.Interaction):
        await interaction.response.defer(ephemeral=True, thinking=True)
        global g_Scoreboard
        try:
            if g_Scoreboard is None:
                await interaction.followup.send(f'There is no open file to save.', ephemeral=True)
                return
            filename = await g_Scoreboard.get_filename()
            await g_Scoreboard.export_json()
            await interaction.followup.send(f'Saved file to {filename}!', ephemeral=True)
        except Exception as e:
            logging.log(logging.WARNING, f'Failed to save file. {e.with_traceback()}.')
            await interaction.followup.send(f'Failed to save file. {e}.', ephemeral=True)
    
    # ------------------------------------------------------------------------------------------------------------------------------
    
    @app_commands.command(name='view')
    @app_commands.describe(for_others='Show to others in channel?')
    @run_by_approved_member()
    async def _view(self, interaction: discord.Interaction, for_others: bool = False):
        await interaction.response.defer(ephemeral=(not for_others), thinking=True)
        global g_Scoreboard
        try:
            if g_Scoreboard is None:
                await interaction.followup.send(f'There is no open file to view.', ephemeral=(not for_others))
                return
            embed = await g_Scoreboard.get_view_embed(for_others)
            embed.set_footer(text="Viewing scoresheet")
            await interaction.followup.send(embed=embed, ephemeral=(not for_others))
        except Exception as e:
            logging.log(logging.WARNING, f'Failed to view scoresheet data. {e.with_traceback()}.')
            await interaction.followup.send(f'Failed to view scoresheet data. {e}.', ephemeral=True)
    
    # ------------------------------------------------------------------------------------------------------------------------------
    
    @app_commands.command(name='win')
    @app_commands.describe(gameid='Game to set winner of', nameid='Person who won')
    @run_by_moderator_or_admin()
    async def _win(self, interaction: discord.Interaction, gameid: int, nameid: int):
        await do_winner(interaction, gameid, nameid)
    
    @_win.autocomplete('gameid')
    async def _win_game_ac(self, interaction: discord.Interaction, current: str) -> List[app_commands.Choice[int]]:
        global g_Scoreboard
        return await g_Scoreboard.get_picks_not_won(current)
    
    @_win.autocomplete('nameid')
    async def _win_name_ac(self, interaction: discord.Interaction, current: str) -> List[app_commands.Choice[int]]:
        global g_Scoreboard
        return await g_Scoreboard.get_names(current)
    # ------------------------------------------------------------------------------------------------------------------------------
    # ------------------------------------------------------------------------------------------------------------------------------
    # ------------------------------------------------------------------------------------------------------------------------------
    
    rules_group = app_commands.Group(name='ruleset', description='Create ruleset images')
    
    # ------------------------------------------------------------------------------------------------------------------------------
    
    @rules_group.command(name='init')
    @app_commands.describe(round='Round of the competition', name1='Name of the first person', name2='Name of the second person')
    @run_by_moderator_or_admin()
    async def _rules_init(self, interaction: discord.Interaction, round: game_ids, name1: str, name2: str):
        await interaction.response.defer(ephemeral=True, thinking=True)
        global g_Manifest
        try:
            filename = f'[R{round}] {name1} vs {name2}.json'
            if os.path.exists(f'{RULESETS_DIR}/{filename}'):
                raise RuntimeError('File to create already exists')
            
            # Create and export new scoreboard
            ruleset = BvBRuleset(g_Manifest)
            await ruleset.set_filename(f'{RULESETS_DIR}/{filename}')
            await ruleset.set_name(1, name1)
            await ruleset.set_name(2, name2)
            await ruleset.export_json()
            
            await interaction.followup.send(f'Created new file {filename}!', ephemeral=True)
        except Exception as e:
            logging.log(logging.WARNING, f'Failed to create {filename}. {e.with_traceback()}.')
            await interaction.followup.send(f'Failed to create {filename}. {e}.', ephemeral=True)
    
    @_rules_init.autocomplete('name1')
    async def _rules_init_name1_ac(self, interaction: discord.Interaction, current: str) -> List[app_commands.Choice[str]]:
        return await get_names_all(current)
    
    @_rules_init.autocomplete('name2')
    async def _rules_init_name2_ac(self, interaction: discord.Interaction, current: str) -> List[app_commands.Choice[str]]:
        return await get_names_all(current)
    
    # ------------------------------------------------------------------------------------------------------------------------------
    
    @rules_group.command(name='blueprint')
    @run_by_moderator_or_admin()
    async def _rules_blueprint(self, interaction: discord.Interaction):
        await interaction.response.defer(ephemeral=True, thinking=True)
        global g_Scoreboard, g_Manifest
        try:
            if g_Scoreboard is None:
                await interaction.followup.send(f'There is no open file to blueprint from.', ephemeral=True)
                return
            filename = (await g_Scoreboard.get_filename()).rpartition("/")[2]
            if os.path.exists(f'{RULESETS_DIR}/{filename}'):
                raise RuntimeError('File to create already exists')
            
            # Create and export new scoreboard
            ruleset = BvBRuleset(g_Manifest)
            await ruleset.set_filename(f'{RULESETS_DIR}/{filename}')
            await ruleset.set_duo(await g_Scoreboard.get_duo())
            await ruleset.set_name(1, await g_Scoreboard.get_name(1))
            await ruleset.set_name(2, await g_Scoreboard.get_name(2))
            for i in range(NUM_GAMES):
                if i >= 6 and i < 9:
                    continue
                await ruleset.set_game(i + 1, await g_Scoreboard.get_game(i + 1))
            await ruleset.export_json()
            
            await interaction.followup.send(f'Created new file {filename}!', ephemeral=True)
        except Exception as e:
            logging.log(logging.WARNING, f'Failed to create {filename}. {e.with_traceback()}.')
            await interaction.followup.send(f'Failed to create {filename}. {e}.', ephemeral=True)
    
    # ------------------------------------------------------------------------------------------------------------------------------
    # ------------------------------------------------------------------------------------------------------------------------------
    # ------------------------------------------------------------------------------------------------------------------------------
    
    set_group = app_commands.Group(name='set', description='Set values directly')
    
    
    # ------------------------------------------------------------------------------------------------------------------------------
    
    @set_group.command(name='name')
    @app_commands.describe(index='Name number to set', name='Person to set as')
    @run_by_moderator_or_admin()
    async def _set_name(self, interaction: discord.Interaction, index: name_ids, name: str):
        await interaction.response.defer(ephemeral=True, thinking=True)
        global g_Scoreboard
        try:
            if g_Scoreboard is None:
                await interaction.followup.send(f'There is no open file to modify.', ephemeral=True)
                return
            await g_Scoreboard.set_name(index, name)
            embed = await g_Scoreboard.get_view_embed()
            embed.set_footer(text=f'Set name {index} to {name}.')
            await interaction.followup.send(embed=embed, ephemeral=True)
        except Exception as e:
            logging.log(logging.WARNING, f'Failed to set name. {e.with_traceback()}.')
            await interaction.followup.send(f'Failed to set name. {e}.', ephemeral=True)
    
    @_set_name.autocomplete('name')
    async def _set_name_ac(self, interaction: discord.Interaction, current: str) -> List[app_commands.Choice[str]]:
        return await get_names_all(current)
    
    # ------------------------------------------------------------------------------------------------------------------------------
    
    @set_group.command(name='duo')
    @app_commands.describe(duo='Name of the duo')
    @run_by_moderator_or_admin()
    async def _set_duo(self, interaction: discord.Interaction, duo: str):
        await interaction.response.defer(ephemeral=True, thinking=True)
        global g_Scoreboard
        try:
            if g_Scoreboard is None:
                await interaction.followup.send(f'There is no open file to modify.', ephemeral=True)
                return
            await g_Scoreboard.set_duo(duo)
            embed = await g_Scoreboard.get_view_embed()
            embed.set_footer(text=f'Set duo to \"{duo}\".')
            await interaction.followup.send(embed=embed, ephemeral=True)
        except Exception as e:
            logging.log(logging.WARNING, f'Failed to set duo: {e.with_traceback()}.')
            await interaction.followup.send(f'Failed to set duo. {e}.', ephemeral=True)
    
    # ------------------------------------------------------------------------------------------------------------------------------
    
    @set_group.command(name='game')
    @app_commands.describe(index='Game number to set', game='Game to set as')
    @run_by_moderator_or_admin()
    async def _set_game(self, interaction: discord.Interaction, index: game_ids, game: str):
        await interaction.response.defer(ephemeral=True, thinking=True)
        global g_Scoreboard
        try:
            if g_Scoreboard is None:
                await interaction.followup.send(f'There is no open file to modify.', ephemeral=True)
                return
            await g_Scoreboard.set_game(index, game)
            embed = await g_Scoreboard.get_view_embed()
            embed.set_footer(text=f'Set game {index} to {game}.')
            await interaction.followup.send(embed=embed, ephemeral=True)
        except Exception as e:
            logging.log(logging.WARNING, f'Failed to set game. {e.with_traceback()}.')
            await interaction.followup.send(f'Failed to set game. {e}.', ephemeral=True)
    
    @_set_game.autocomplete('game')
    async def _set_game_ac(self, interaction: discord.Interaction, current: str) -> List[app_commands.Choice[str]]:
        global g_Scoreboard
        keys = await g_Scoreboard.manifest.get_keys()
        return_obj = []
        for k in keys:
            if len(return_obj) >= 25:
                break
            display_name = await g_Scoreboard.manifest.get_displayname(k)
            if current.lower() in display_name.lower():
                return_obj.append(app_commands.Choice(name=display_name, value=k))
        return return_obj
    
    # ------------------------------------------------------------------------------------------------------------------------------
    
    @set_group.command(name='winner')
    @app_commands.describe(pickid='Game to set winner of', nameid='Person who won')
    @run_by_moderator_or_admin()
    async def _set_winner(self, interaction: discord.Interaction, pickid: int, nameid: int):
        global g_Scoreboard
        await do_winner(interaction, 1 + await g_Scoreboard.get_gameid_from_pickid(pickid), nameid)
    
    @_set_winner.autocomplete('pickid')
    async def _set_winner_game_ac(self, interaction: discord.Interaction, current: str) -> List[app_commands.Choice[int]]:
        global g_Scoreboard
        return await g_Scoreboard.get_picks(current)
    
    @_set_winner.autocomplete('nameid')
    async def _set_winner_name_ac(self, interaction: discord.Interaction, current: str) -> List[app_commands.Choice[int]]:
        global g_Scoreboard
        return await g_Scoreboard.get_names(current)
    
    # ------------------------------------------------------------------------------------------------------------------------------
    
    @set_group.command(name='ban')
    @app_commands.describe(gameid='Game to ban')
    @run_by_moderator_or_admin()
    async def _set_ban(self, interaction: discord.Interaction, gameid: int):
        await do_ban(interaction, gameid)
    
    @_set_ban.autocomplete('gameid')
    async def _set_ban_ac(self, interaction: discord.Interaction, current: str) -> List[app_commands.Choice[int]]:
        global g_Scoreboard
        return await g_Scoreboard.get_games(current)
            
    # ------------------------------------------------------------------------------------------------------------------------------
    
    @set_group.command(name='pick')
    @app_commands.describe(pickid='Position to pick game in', gameid='Game to pick')
    @run_by_moderator_or_admin()
    async def _set_pick(self, interaction: discord.Interaction, pickid: choice_ids, gameid: int):
        await interaction.response.defer(ephemeral=True, thinking=True)
        global g_Scoreboard
        try:
            if g_Scoreboard is None:
                await interaction.followup.send(f'There is no open file to modify.', ephemeral=True)
                return
            await g_Scoreboard.set_pick(pickid, gameid)
            game_name = await g_Scoreboard.get_game_display(gameid)
            embed = await g_Scoreboard.get_view_embed()
            embed.set_footer(text=f'Picked {game_name} as game {pickid}.')
            await interaction.followup.send(embed=embed, ephemeral=True)
        except Exception as e:
            logging.log(logging.WARNING, f'Failed to pick game. {e.with_traceback()}.')
            await interaction.followup.send(f'Failed to pick game. {e}.', ephemeral=True)
    
    @_set_pick.autocomplete('gameid')
    async def _set_pick_ac(self, interaction: discord.Interaction, current: str) -> List[app_commands.Choice[int]]:
        global g_Scoreboard
        return await g_Scoreboard.get_games(current)
            
    # ------------------------------------------------------------------------------------------------------------------------------
    
    @set_group.command(name='chooser')
    @app_commands.describe(pickid='Game to set chooser of', nameid='Person who picked the game')
    @run_by_moderator_or_admin()
    async def _set_chooser(self, interaction: discord.Interaction, pickid: int, nameid: int):
        await interaction.response.defer(ephemeral=True, thinking=True)
        global g_Scoreboard
        try:
            if g_Scoreboard is None:
                await interaction.followup.send(f'There is no open file to modify.', ephemeral=True)
                return
            await g_Scoreboard.set_chooser(pickid, nameid)
            game_name = await g_Scoreboard.get_pick_display(pickid)
            name      = await g_Scoreboard.get_name(nameid)
            embed     = await g_Scoreboard.get_view_embed()
            embed.set_footer(text=f'Set {name} as chooser for {game_name}.')
            await interaction.followup.send(embed=embed, ephemeral=True)
        except Exception as e:
            logging.log(logging.WARNING, f'Failed to set chooser. {e.with_traceback()}.')
            await interaction.followup.send(f'Failed to set chooser. {e}.', ephemeral=True)
    
    @_set_chooser.autocomplete('pickid')
    async def _set_chooser_pick_ac(self, interaction: discord.Interaction, current: str) -> List[app_commands.Choice[int]]:
        global g_Scoreboard
        return await g_Scoreboard.get_picks(current)
    
    @_set_chooser.autocomplete('nameid')
    async def _set_chooser_name_ac(self, interaction: discord.Interaction, current: str) -> List[app_commands.Choice[int]]:
        global g_Scoreboard
        return await g_Scoreboard.get_names(current)
    
    # ------------------------------------------------------------------------------------------------------------------------------

async def do_ban(interaction: discord.Interaction, gameid: int):
    await interaction.response.defer(ephemeral=True, thinking=True)
    global g_Scoreboard
    try:
        if g_Scoreboard is None:
            await interaction.followup.send(f'There is no open file to modify.', ephemeral=True)
            return
        await g_Scoreboard.toggle_ban(gameid)
        game_name = await g_Scoreboard.get_game_display(gameid)
        embed = await g_Scoreboard.get_view_embed()
        if await g_Scoreboard.is_banned(gameid):
            embed.set_footer(text=f'Banned {game_name}.')
        else:
            embed.set_footer(text=f'Unbanned {game_name}.')
        await interaction.followup.send(embed=embed, ephemeral=True)
    except Exception as e:
        logging.log(logging.WARNING, f'Failed to toggle ban. {e.with_traceback()}.')
        await interaction.followup.send(f'Failed to toggle ban. {e}.', ephemeral=True)

async def do_winner(interaction: discord.Interaction, gameid: int, nameid: int):
    await interaction.response.defer(ephemeral=True, thinking=True)
    global g_Scoreboard
    try:
        if g_Scoreboard is None:
            await interaction.followup.send(f'There is no open file to modify.', ephemeral=True)
            return
        await g_Scoreboard.set_win(gameid, nameid)
        game_name = await g_Scoreboard.get_game_display(gameid)
        name      = await g_Scoreboard.get_name(nameid)
        embed     = await g_Scoreboard.get_view_embed()
        embed.set_footer(text=f'Set winner of {game_name} to {name}.')
        await interaction.followup.send(embed=embed, ephemeral=True)
    except Exception as e:
        logging.log(logging.WARNING, f'Failed to set winner. {e.with_traceback()}.')
        await interaction.followup.send(f'Failed to set winner. {e}.', ephemeral=True)

# Returns a set of choices from the global list of names with:
#   Display - Name
#   Value   - Name
async def get_names_all(current: str) -> List[app_commands.Choice[str]]:
    global g_Names
    return_obj = [app_commands.Choice(name=name, value=name) for name in g_Names if current.lower() in name.lower()]
    return return_obj[:min(len(return_obj), 25)]