from asyncio import Lock
import discord
from discord.ext import commands
from dotenv import load_dotenv
import DiscordScoreSheet
import GamesManifest
import logging
import os
import sys
from typing import List, Optional

Z_DEBUG = 25

g_GuildID = 0
g_MemberList: List[discord.Member]
g_EmojiLock = Lock()
g_Emojis: List[discord.Emoji] = None

def main(log_mode: int = 25) -> None:
    global g_GuildID
    
    # Setup logging functionality
    logging.basicConfig(filename='dasapbvb.log', encoding='utf-8', level=log_mode, format='%(asctime)s [%(levelname)s]: %(message)s', datefmt='%Y-%m-%d %I:%M:%S')
    logging.addLevelName(25, 'Z_DEBUG')
    
    # Load environment variables for secret IDs
    load_dotenv()
    TOKEN = os.getenv('DISCORD_TOKEN')
    g_GuildID = int(os.getenv('GUILD_ID'))
    DiscordScoreSheet.g_AdminID = int(os.getenv('ADMIN_ID'))
    DiscordScoreSheet.g_ModID   = int(os.getenv('MODERATOR_ID'))
    DiscordScoreSheet.g_GamerID = int(os.getenv('GAMER_ID'))
    DiscordScoreSheet.g_GuestID = int(os.getenv('GUEST_ID'))
    DiscordScoreSheet.g_Names = os.getenv('BVB_NAMES').split(',')
    
    # Create bot object with all intents
    intents = discord.Intents.all()
    bot = commands.Bot(command_prefix='!', intents=intents)
    
    # Define event response for bot connecting to guild
    @bot.event
    async def on_ready():
        global g_MemberList, g_GuildID
        member: discord.Member
        this_guild: Optional[discord.Guild]
        
        logging.log(Z_DEBUG, f'{bot.user} connected to Discord.')
        
        this_guild = bot.get_guild(g_GuildID)
        if this_guild is not None:
            logging.log(Z_DEBUG, f'Got guild {this_guild.name} with ID {this_guild.id}.')
        else:
            logging.CRITICAL(f'Failed to get Bro vs Bro guild.')
            exit(-1)
        
        # Sync application commands with the guild
        await bot.tree.sync(guild=discord.Object(id=g_GuildID))
        print(f'GetCommands {bot.tree.get_commands(guild=discord.Object(id=g_GuildID))}')
        DiscordScoreSheet.g_Manifest = GamesManifest.GamesManifest()
        await DiscordScoreSheet.g_Manifest.update(DiscordScoreSheet.GAMES_JSON)
    
    # Add application command set for scoresheet application and run bot
    bot.tree.add_command(DiscordScoreSheet.ScoreSheet(bot=bot, name="score"), guild=discord.Object(id=g_GuildID))
    bot.run(TOKEN)

if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1] == '--debug':
        main(logging.DEBUG)
    else:
        main()
