from typing import Any, Callable, Collection, List, Optional, Tuple, TypeVar, Union


X = TypeVar('X')
Y = TypeVar('Y')

# Makes a deep copy of elements from a source array into a destination array.
# Allows an optional 'n' parameter, which defines the number of elements to copy
#   (by default, the function will try to copy all of the source elements).
def array_copy(dest: List[X], src: List[X], n: Optional[int] = None) -> None:
    dest_len = len(dest)
    dest_src = n if n is not None else len(src)
    
    if dest_len < dest_src:
        RuntimeError("Destination array is too small to receive source elements.")
        
    for i in range(dest_src):
        dest[i] = src[i]


# Performs a bisection search for 'search' in 'arr' for generic types. For multiple
#   successful items in 'arr', returns the index of the left-most item.
# Allows an optional comparison function, where 'comp_func' must return -1, 0, or 1
#   based on the relative ordering of the search and arr objects. By default, it 
#   simply uses arr > search -> -1, arr == search -> 0, arr < search -> 1.
# Also allows an optional map function, to allow 'arr' values to be mapped to the
#    same type as 'search' before the comparison is applied.
def bisection(arr: Collection[X], search: Y,  comp_func: Optional[Callable[[Y, Y], int]] = lambda x, y: 1 if x > y else 0 if x == y else -1, map_func: Optional[Callable[[X], Y]] = lambda x: x) -> int:
    L = 0
    R = len(arr)
    while(L < R):
        m = (L + R) // 2
        val = map_func(arr[m])
        if(comp_func(val, search) == -1):
            L = m + 1
        else:
            R = m
    if(L < len(arr) and comp_func(map_func(arr[L]), search) == 0):
        return L
    else:
        return -1


# Determines whether a Collection is correctly unpacked from a containing Tuple or List,
#   and unpacks it if not.
# Useful for when returning Collections from Dictionaries.
def unpack_tuple(obj: Union[List[X], Tuple[List[X]], List[List[X]]]) -> List[X]:
    if len(obj) == 1 and type(obj[0]) is list:
        return obj[0]
    else:
        return obj