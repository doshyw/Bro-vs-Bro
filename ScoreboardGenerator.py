from asyncio import Lock
from enum import Enum
from PIL import Image, ImageDraw, ImageFont, ImageOps

BASE_FOLDER = 'scoreboards/base'
SCOREBOARD_IMG = 'empty_scoreboard.jpeg'
BANNED_IMG = 'banned.png'

COLORIZE_BLUE  = ( 74, 141, 201,  170)
COLORIZE_RED   = (210,  19,   4,  170)
COLORIZE_BLUE_CHOOSER  = ( 74, 141, 201,  170)
COLORIZE_RED_CHOOSER   = (210,  19,   4,  170)
COLORIZE_BLACK = (  0,   0,   0)
COLORIZE_GREY  = (127, 127, 127)

FONT_NAME = ImageFont.truetype(f'{BASE_FOLDER}/Benzin Bold.ttf', 48)
FONT_DUO  = ImageFont.truetype(f'{BASE_FOLDER}/Benzin Bold.ttf', 36)
FONT_PICK = ImageFont.truetype(f'{BASE_FOLDER}/Benzin Bold.ttf', 24)

FILL_BLUE = (136, 200, 255)
FILL_RED  = (255, 197, 179)
FILL_DUO  = (255, 255, 255)

PICKS_BBOX_X_A     = 87
PICKS_BBOX_X_B     = 793
PICKS_BBOX_X_C     = 1165
PICKS_BBOX_Y       = 488
PICKS_BBOX_WIDTH_A = 675
PICKS_BBOX_WIDTH_B = 342
PICKS_BBOX_WIDTH_C = 675
PICKS_BBOX_HEIGHT  = 122
PICKS_SIZE         = 80
PICKS_COUNT_A      = 6
PICKS_COUNT_B      = 3
PICKS_COUNT_C      = 6
PICKS_SEPARATOR_A  = (PICKS_BBOX_WIDTH_A - PICKS_SIZE*PICKS_COUNT_A) // (PICKS_COUNT_A + 1)
PICKS_SEPARATOR_B  = (PICKS_BBOX_WIDTH_B - PICKS_SIZE*PICKS_COUNT_B) // (PICKS_COUNT_B + 1)
PICKS_SEPARATOR_C  = (PICKS_BBOX_WIDTH_C - PICKS_SIZE*PICKS_COUNT_C) // (PICKS_COUNT_C + 1)
PICKS_MARGIN_X_A   = (PICKS_BBOX_WIDTH_A - PICKS_SEPARATOR_A*(PICKS_COUNT_A - 1) - PICKS_SIZE*PICKS_COUNT_A) // 2
PICKS_MARGIN_X_B   = (PICKS_BBOX_WIDTH_B - PICKS_SEPARATOR_B*(PICKS_COUNT_B - 1) - PICKS_SIZE*PICKS_COUNT_B) // 2
PICKS_MARGIN_X_C   = (PICKS_BBOX_WIDTH_C - PICKS_SEPARATOR_C*(PICKS_COUNT_C - 1) - PICKS_SIZE*PICKS_COUNT_C) // 2
PICKS_MARGIN_Y     = (PICKS_BBOX_HEIGHT - PICKS_SIZE) // 2

CHOICES_BBOX_X       = 215
CHOICES_BBOX_Y       = 718
CHOICES_BBOX_WIDTH   = 1508
CHOICES_BBOX_HEIGHT  = 179
CHOICES_SIZE         = 120
CHOICES_SIZE_CHOOSER = 40
CHOICES_COUNT        = 9
CHOICES_SEPARATOR    = (CHOICES_BBOX_WIDTH - CHOICES_SIZE*CHOICES_COUNT) // (CHOICES_COUNT + 1)
CHOICES_MARGIN_X     = (CHOICES_BBOX_WIDTH - CHOICES_SEPARATOR*(CHOICES_COUNT - 1) - CHOICES_SIZE*CHOICES_COUNT) // 2
CHOICES_MARGIN_Y     = (CHOICES_BBOX_HEIGHT - CHOICES_SIZE) // 2

NAMES_BBOX_X         = 86
NAMES_BBOX_Y         = 337
NAMES_BBOX_WIDTH     = 1753
NAMES_BBOX_HEIGHT    = 122
NAMES_OFFSET_OUTSIDE = 130
NAMES_OFFSET_TEXT    = 26
NAMES_OFFSET_TOP     = 18
NAMES_SIZE           = 80
NAMES_OFFSET_CLINE   = 2 + NAMES_BBOX_HEIGHT // 2

class Greyout(Enum):
    NONE = 0
    GREY = 1
    BLUE = 2
    RED  = 3

class ScoreboardGenerator:
    lock: Lock
    
    def __init__(self: 'ScoreboardGenerator', background_img: str):
        ###
        ### TODO: Need to check that the background image exists
        ###
        temp = Image.open(background_img)
        self.img: Image.Image = Image.new("RGBA", temp.size)
        self.img.paste(temp)
        self.lock = Lock()
    
    async def overlay_game(self: 'ScoreboardGenerator', game_file: str, index: int, is_banned: bool, pick_index: int, greyout: Greyout) -> None:
        ###
        ### TODO: Need to check that the image exists
        ###
        
        async with self.lock:
            # Generate an RGBA image for the picked game.
            temp = Image.open(game_file)
            pick_img = Image.new("RGBA", temp.size)
            pick_img.paste(temp)
            
            # Scales the pick image to the appropriate height, and crops from the right if not square.
            pick_img = ImageOps.scale(pick_img, PICKS_SIZE / pick_img.height)
            pick_img = pick_img.crop((0, 0, pick_img.height, pick_img.height))
            
            # Checks if the image needs to be greyed out / colorised, and applies the coloring.
            if greyout != Greyout.NONE:
                if greyout == Greyout.GREY:
                    temp = ImageOps.grayscale(pick_img)
                    temp = ImageOps.colorize(temp, COLORIZE_BLACK, COLORIZE_GREY)
                    pick_img.paste(temp)
                elif greyout == Greyout.BLUE:
                    temp = Image.new('RGBA', pick_img.size, COLORIZE_BLUE)
                    pick_img.alpha_composite(temp)
                elif greyout == Greyout.RED:
                    temp = Image.new('RGBA', pick_img.size, COLORIZE_RED)
                    pick_img.alpha_composite(temp)
            
            # Checks if the image has been banned, and overlays a ban image if so.
            if is_banned:
                temp = Image.open(f'{BASE_FOLDER}/{BANNED_IMG}')
                temp = ImageOps.scale(temp, pick_img.height / temp.height)
                pick_img = Image.alpha_composite(pick_img, temp)
            
            # Calculates the location offset at which to place the pick image, based on its index.
            if index < 0 or index > 14:
                raise RuntimeError(f"ScoreboardGenerator.overlay_game({game_file}, {index}, {is_banned}, {greyout.name}) - index is out of bounds")
            elif index < 6:
                location = (PICKS_BBOX_X_A + PICKS_MARGIN_X_A + index*(PICKS_SIZE + PICKS_SEPARATOR_A),
                    PICKS_BBOX_Y + PICKS_MARGIN_Y)
            elif index < 9:
                location = (PICKS_BBOX_X_B + PICKS_MARGIN_X_B + (index - 6)*(PICKS_SIZE + PICKS_SEPARATOR_B),
                    PICKS_BBOX_Y + PICKS_MARGIN_Y)
            else:
                location = (PICKS_BBOX_X_C + PICKS_MARGIN_X_C + (index - 9)*(PICKS_SIZE + PICKS_SEPARATOR_C),
                    PICKS_BBOX_Y + PICKS_MARGIN_Y)
            
            # Pastes the final image onto the canvas.
            self.img.paste(pick_img, location, pick_img)
            
            ###
            ### TODO: Remove hardcoding of pick index upper-bound
            ###
            if pick_index > 0 and pick_index < 10:
                location = (location[0] + PICKS_SIZE - 5, location[1] + 5)
                draw = ImageDraw.Draw(self.img, "RGBA")
                draw.text(xy=location, text=str(pick_index), fill=(255, 255, 255), font=FONT_PICK, anchor="ra")
    
    async def overlay_pick(self: 'ScoreboardGenerator', game_file: str, index: int, greyout: Greyout, chooser_file: str, greyout_chooser: Greyout) -> None:
        ###
        ### TODO: Need to check that the image exists
        ###
        
        async with self.lock:
            # Generate an RGBA image for the picked game.
            temp = Image.open(game_file)
            choice_img = Image.new("RGBA", temp.size)
            choice_img.paste(temp)
            
            # Scales the pick image to the appropriate height, and crops from the right if not square.
            choice_img = ImageOps.scale(choice_img, CHOICES_SIZE / choice_img.height)
            choice_img.crop((0, 0, choice_img.height, choice_img.height))
            
            # Checks if the image needs to be greyed out / colorised, and applies the coloring.
            if greyout != Greyout.NONE:
                if greyout == Greyout.GREY:
                    temp = ImageOps.grayscale(choice_img)
                    temp = ImageOps.colorize(temp, COLORIZE_BLACK, COLORIZE_GREY)
                    choice_img.paste(temp)
                elif greyout == Greyout.BLUE:
                    temp = Image.new('RGBA', choice_img.size, COLORIZE_BLUE)
                    choice_img.alpha_composite(temp)
                elif greyout == Greyout.RED:
                    temp = Image.new('RGBA', choice_img.size, COLORIZE_RED)
                    choice_img.alpha_composite(temp)
            
            ###
            ### TODO: Need to put chooser image into the corner
            ###
            if chooser_file != '':
                temp = Image.open(chooser_file)
                chooser_img = Image.new("RGBA", temp.size)
                chooser_img.paste(temp)
                
                # Scales the chooser image to the appropriate height, and crops from the right if not square.
                chooser_img = ImageOps.scale(chooser_img, CHOICES_SIZE_CHOOSER / chooser_img.height)
                chooser_img.crop((0, 0, chooser_img.height, chooser_img.height))
                
                if greyout_chooser != Greyout.NONE:
                    if greyout_chooser == Greyout.BLUE:
                        temp = Image.new('RGBA', chooser_img.size, COLORIZE_BLUE_CHOOSER)
                        chooser_img.alpha_composite(temp)
                    if greyout_chooser == Greyout.RED:
                        temp = Image.new('RGBA', chooser_img.size, COLORIZE_RED_CHOOSER)
                        chooser_img.alpha_composite(temp)
                
                choice_img.paste(chooser_img, (choice_img.width - chooser_img.width, 0), chooser_img)
            
            # Calculates the location offset at which to place the pick image, based on its index.
            if index < 0 or index > 9:
                raise RuntimeError(f"ScoreboardGenerator.overlay_pick({game_file}, {index}, {chooser_file}, {greyout.name}) - index is out of bounds")
            else:
                location = (CHOICES_BBOX_X + CHOICES_MARGIN_X + index*(CHOICES_SIZE + CHOICES_SEPARATOR),
                CHOICES_BBOX_Y + CHOICES_MARGIN_Y)
            
            # Pastes the final image onto the canvas.
            self.img.paste(choice_img, location, choice_img)
            
            

    async def overlay_name(self: 'ScoreboardGenerator', name_file: str, name: str, index: int):
        ###
        ### TODO: Need to check that the image exists
        ###
        
        async with self.lock:
            # Generate an RGBA image for the picked game.
            temp = Image.open(name_file)
            name_img = Image.new("RGBA", temp.size)
            name_img.paste(temp)
            
            # Scales the image to the appropriate height, and crops from the right if not square.
            name_img = ImageOps.scale(name_img, NAMES_SIZE / name_img.height)
            name_img = name_img.crop((0, 0, name_img.height, name_img.height))
            
            if index == 0:
                # Pastes the image onto the canvas.
                location = (NAMES_BBOX_X + NAMES_OFFSET_OUTSIDE, NAMES_BBOX_Y + NAMES_OFFSET_TOP)
                self.img.paste(name_img, location, name_img)
                
                location = (NAMES_BBOX_X + NAMES_OFFSET_OUTSIDE + NAMES_SIZE + NAMES_OFFSET_TEXT, 
                    NAMES_BBOX_Y + NAMES_OFFSET_CLINE)
                draw = ImageDraw.Draw(self.img, "RGBA")
                draw.text(xy=location, text=name.upper(), fill=FILL_BLUE, font=FONT_NAME, anchor="lm")
                
            elif index == 1:
                # Pastes the image onto the canvas.
                location = (NAMES_BBOX_X + NAMES_BBOX_WIDTH - (NAMES_SIZE + NAMES_OFFSET_OUTSIDE), NAMES_BBOX_Y + NAMES_OFFSET_TOP)
                self.img.paste(name_img, location, name_img)
                
                location = (NAMES_BBOX_X + NAMES_BBOX_WIDTH - (NAMES_SIZE + NAMES_OFFSET_OUTSIDE + NAMES_OFFSET_TEXT), 
                    NAMES_BBOX_Y + NAMES_OFFSET_CLINE)
                draw = ImageDraw.Draw(self.img, "RGBA")
                draw.text(xy=location, text=name.upper(), fill=FILL_RED, font=FONT_NAME, anchor="rm")
            else:
                raise RuntimeError(f"ScoreboardGenerator.overlay_name({name_file}, {name}, {index}) - index is out of bounds")
    
    async def overlay_duo(self: 'ScoreboardGenerator', duo: str):
        async with self.lock:
            location = (self.img.width // 2, NAMES_BBOX_Y + NAMES_OFFSET_CLINE)
            draw = ImageDraw.Draw(self.img, "RGBA")
            draw.text(xy=location, text=f'"{duo.upper()}"', fill=FILL_DUO, font=FONT_DUO, anchor="mm")
    
    async def export_img(self: 'ScoreboardGenerator', img_location: str):
        async with self.lock:
            self.img.save(img_location)