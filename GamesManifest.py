from aiorwlock import RWLock
from typing import Any, Dict, Iterable
import json
import os

GAMES_FOLDER = 'games'

class GamesManifest:
    json_location: str
    json_content: Dict[str, Any]
    lock: RWLock
    
    def __init__(self: 'GamesManifest'):
        self.lock = RWLock()
    
    async def recalculate(self: 'GamesManifest', json_location: str = None):
        async with self.lock.writer_lock:
            if json_location is not None:
                self.json_location = json_location
            
            if os.path.exists(self.json_location):
                with open(self.json_location, 'r') as f:
                    output_file: Dict[str, Any] = json.load(f)
            
            cache = [x[0] for x in output_file.values()]

            for file in os.listdir(GAMES_FOLDER):
                if file.endswith('.PNG'):
                    if file not in cache:
                        output_file[file.removesuffix('.PNG').lower()] = (file, file.removesuffix('.PNG'))
                if file.endswith('.png'):
                    if file not in cache:
                        output_file[file.removesuffix('.png').lower()] = (file, file.removesuffix('.png'))

            sorted_file = {}
            for name in sorted(output_file):
                sorted_file[name] = output_file[name]

            with open(self.json_location, 'w') as f:
                json.dump(sorted_file, f, indent=1, separators=(', ', ': '))
        
        await self.update()
    
    async def update(self: 'GamesManifest', json_location: str = None):
        # If another thread is currently running update(), wait for it to complete
        # and then return without running update again.       
        async with self.lock.writer_lock:
            if json_location is not None:
                self.json_location = json_location
                
            if os.path.exists(self.json_location):
                with open(self.json_location, 'r') as f:
                    self.json_content = json.load(f)
            else:
                raise RuntimeError(f'GamesManifest.update({self.json_location}) - Cannot find file at location specified')
    
    async def check_game(self: 'GamesManifest', game: str) -> bool:
        async with self.lock.reader_lock:
            if game is None:
                return False
            else:
                return game.lower() in self.json_content.keys()
    
    async def get_keys(self: 'GamesManifest') -> Iterable[str]:
        async with self.lock.reader_lock:
            return self.json_content.keys()
    
    async def get_filename(self: 'GamesManifest', game: str) -> str:
        async with self.lock.reader_lock:
            if await self.check_game(game):
                return self.json_content[game][0]
            else:
                raise RuntimeError(f'GamesManifest.get_filename({game}) - key does not exist in dictionary')
    
    async def get_displayname(self: 'GamesManifest', game: str) -> str:
        async with self.lock.reader_lock:
            if await self.check_game(game):
                return self.json_content[game][1]
            else:
                raise RuntimeError(f'GamesManifest.get_displayname({game}) - key does not exist in dictionary')