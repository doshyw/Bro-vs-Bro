from aiorwlock import RWLock
from copy import deepcopy
import discord
from discord import app_commands
from GamesManifest import GamesManifest, GAMES_FOLDER
import json
import os
from operator import indexOf
from ScoreboardGenerator import BASE_FOLDER, Greyout, SCOREBOARD_IMG, ScoreboardGenerator
from typing import Any, Dict, List, Literal, Optional
from zbdsh_lib import unpack_tuple

NUM_GAMES = 15
NUM_PICKS = 9

DEFAULT_IMG = f'{BASE_FOLDER}/placeholder.png'
PEOPLE_FOLDER = 'people'

number_emojis = ["1️⃣", "2️⃣", "3️⃣", "4️⃣", "5️⃣", "6️⃣", "7️⃣", "8️⃣", "9️⃣"]

# JSON format for scoring
# Duo:             str - Name of the duo
# Names:    List [str] - Names of the two players
# Games:    List [str] - Names of the 15 games
# Winners:  List [int] - IDs of the winners of each of the 15 games
# Bans:     List[bool] - IDs of the games banned
# Picks:    List [int] - IDs of the games picked
# Choosers: List [int] - IDs of the players who chose each game

class BvBScoreboard:
    lock: RWLock
    generator: ScoreboardGenerator
    
    # Initialises the BvBScoreboard object with a base set of properties
    def __init__(self: 'BvBScoreboard', manifest: GamesManifest):
        self.duo:       Optional[str]        =   None              # Name of the duo
        self.names:     List[Optional[str]]  =  [None] * 2         # Names of the two players
        self.games:     List[str]            =  [None] * NUM_GAMES # Names of the 15 games
        self.winners:   List[int]            =    [-1] * NUM_GAMES # IDs of the winners of each of the 15 games
        self.bans:      List[bool]           = [False] * NUM_GAMES # IDs of the games banned
        self.picks:     List[int]            =    [-1] * NUM_PICKS # IDs of the games picked
        self.choosers:  List[int]            =    [-1] * NUM_PICKS # IDs of the players who chose each game
        self.num_picked = 0
        self.filename = ''
        self.manifest = manifest
        self.lock = RWLock()
        self.generator = ScoreboardGenerator(f'{BASE_FOLDER}/{SCOREBOARD_IMG}')
    
    # Imports the values from the stored JSON object that this object can export
    async def import_json(self, json_location: str) -> None:
        async with self.lock.writer_lock:
            if os.path.exists(json_location):
                with open(json_location, 'r') as f:
                    json_dict = json.load(f)
            else:
                raise RuntimeError(f'import_json({json_location}) - Could not find file at location specified')
            
            self.duo = json_dict['Duo']
            self.names =    deepcopy(unpack_tuple(json_dict['Names']))
            self.games =    deepcopy(unpack_tuple(json_dict['Games']))
            self.winners =  deepcopy(unpack_tuple(json_dict['Winners']))
            self.bans =     deepcopy(unpack_tuple(json_dict['Bans']))
            self.picks =    deepcopy(unpack_tuple(json_dict['Picks']))
            self.choosers = deepcopy(unpack_tuple(json_dict['Choosers']))
            self.filename = json_location
            
            acc = 9
            for i in range(NUM_PICKS - 1, -1, -1):
                if self.picks[i] == -1:
                    acc -= 1
                else:
                    break
            self.num_picked = acc

    
    # Exports the values in the BvBScoreboard object to a JSON object that can be later imported
    async def export_json(self, json_location: str = None) -> None:
        async with self.lock.reader_lock:
            output_json : Dict[str, Any] = {}
            
            output_json['Duo'] =      self.duo
            output_json['Names'] =    deepcopy(self.names)
            output_json['Games'] =    deepcopy(self.games)
            output_json['Winners'] =  deepcopy(self.winners)
            output_json['Bans'] =     deepcopy(self.bans)
            output_json['Picks'] =    deepcopy(self.picks)
            output_json['Choosers'] = deepcopy(self.choosers)
            
            if json_location is None:
                json_location = self.filename
            
            with open(json_location, 'w') as f:
                json.dump(output_json, f, indent=1, separators=(', ', ': '))
            
            os.chmod(json_location, 0o774)
    
    
    async def export_img(self, output_location: str = None) -> None:
        async with self.lock.reader_lock:
            if output_location is None:
                output_location = f"{self.filename.removesuffix('.json')}.png"
            
            if self.duo is not None:
                await self.generator.overlay_duo(self.duo)
                
            for i in range(2):
                if self.names[i] is not None:
                    await self.generator.overlay_name(f'{PEOPLE_FOLDER}/{self.names[i]}.png', self.names[i], i)
                else:
                    await self.generator.overlay_name(DEFAULT_IMG, '', i)
            
            for i in range(NUM_GAMES):
                # If the game at the ID exists, get its filename, otherwise use the default.
                if await self.manifest.check_game(self.games[i]):
                    filename = f'{GAMES_FOLDER}/{await self.manifest.get_filename(self.games[i])}'
                else:
                    filename = DEFAULT_IMG
                
                # Get the pick ID for the game
                pickID = await self.get_pickid_from_gameid(i + 1)
                
                # If the game was banned, or it hasn't been picked and the match is over, grey it out.
                if self.bans[i] or (pickID == -1 and await self.check_win() != -1):
                    greyout = Greyout.GREY
                # If the game hasn't been picked and the match is still going, 
                # or the game has been picked but not won, don't grey it out.
                elif pickID == -1 or self.winners[i] == -1:
                    greyout = Greyout.NONE
                # Otherwise, the game has been picked and won by someone, colour it in their colour.
                else:
                    greyout = Greyout(self.winners[i] + 2)
                
                # Overlay the pick on the base image.
                await self.generator.overlay_game(filename, i, self.bans[i], pickID + 1, greyout)
            
            for i in range(NUM_PICKS):
                # If the game at the ID exists, get its filename, otherwise use the default.
                if self.picks[i] < 0 or self.picks[i] >= NUM_GAMES:
                    filename = DEFAULT_IMG
                else:
                    if await self.manifest.check_game(self.games[self.picks[i]]):
                        filename = f'{GAMES_FOLDER}/{await self.manifest.get_filename(self.games[self.picks[i]])}'
                    else:
                        filename = DEFAULT_IMG
                
                # If no game has been picked, and the match is over, grey out the slot.
                if self.picks[i] == -1 and await self.check_win() != -1:
                    greyout = Greyout.GREY
                # If the game hasn't been picked and the match is still going, 
                # or the game has been picked but not won, don't grey it out.
                elif self.picks[i] == -1 or self.winners[self.picks[i]] == -1:
                    greyout = Greyout.NONE
                # Otherwise, the game has been picked and won by someone, colour it in their colour.
                else:
                    greyout = Greyout(self.winners[self.picks[i]] + 2)
                
                # Pass an empty string for the chooser if no pick has been made yet,
                # otherwise pass the file location of the player image
                if self.choosers[i] == -1:
                    chooser = ''
                    chooser_greyout = Greyout.NONE
                else:
                    chooser = f'{PEOPLE_FOLDER}/{self.names[self.choosers[i]]}.png'
                    if self.choosers[i] == 0:
                        chooser_greyout = Greyout.BLUE
                    else:
                        chooser_greyout = Greyout.RED
                
                # Overlay the pick on the base image.
                await self.generator.overlay_pick(filename, i, greyout, chooser, chooser_greyout)
            
            await self.generator.export_img(output_location)
    
    
    async def get_filename(self) -> str:
        async with self.lock.reader_lock:
            return self.filename
    
    
    async def get_pickid_from_gameid(self, gameID: int):
        async with self.lock.reader_lock:
            if gameID < 1 or gameID > NUM_GAMES:
                raise RuntimeError(f'Index {gameID} for Game is out of bounds')
            if gameID - 1 in self.picks:
                return indexOf(self.picks, gameID - 1)
            else:
                return -1
    
    
    async def get_gameid_from_pickid(self, pickID: int):
        async with self.lock.reader_lock:
            if pickID < 1 or pickID > NUM_PICKS:
                raise RuntimeError(f'Index {pickID} for Game is out of bounds')
            return self.picks[pickID - 1]
    
    
    async def get_num_picked(self):
        async with self.lock.reader_lock:
            return self.num_picked
    
    
    # Maps a GameID to its Game in the self.games field
    async def get_game(self, gameID: int):
        async with self.lock.reader_lock:
            if gameID < 1 or gameID > NUM_GAMES:
                raise RuntimeError(f"GameID {gameID} out of bounds")
            else:
                return self.games[gameID - 1]
    
    
    # Maps a Game to its first appearing GameID in the self.games field
    async def get_game_id(self, game: str) -> int:
        async with self.lock.reader_lock:
            if game not in self.games:
                raise RuntimeError(f"Game {game} could not be found.")
            else:
                return indexOf(self.games, game)
    
    
    # Maps a GameID to its display name
    async def get_game_display(self, gameID: int) -> str:
        async with self.lock.reader_lock:
            if gameID < 1 or gameID > NUM_GAMES:
                raise RuntimeError(f"GameID {gameID} out of bounds")
            else:
                return await self.manifest.get_displayname(self.games[gameID - 1])
    
    
    # Maps a PickID to its display name
    async def get_pick_display(self, pickID: int) -> str:
        async with self.lock.reader_lock:
            if pickID < 1 or pickID > NUM_PICKS:
                raise RuntimeError(f"PickID {pickID} out of bounds")
            else:
                gameID = self.picks[pickID - 1]
                if gameID < 1 or gameID > NUM_GAMES:
                    raise RuntimeError(f"GameID {gameID} out of bounds")
                return await self.manifest.get_displayname(self.games[gameID])
    
    
    # Maps a GameID to its ban value
    async def is_banned(self, gameID: int) -> bool:
        async with self.lock.reader_lock:
            if gameID < 1 or gameID > NUM_GAMES:
                raise RuntimeError(f"GameID {gameID} out of bounds")
            else:
                return self.bans[gameID - 1]
    
    
    # Maps a NameID to its Name
    async def get_duo(self) -> str:
        async with self.lock.reader_lock:
            return self.duo
    
    
    # Maps a NameID to its Name
    async def get_name(self, nameID: int) -> str:
        async with self.lock.reader_lock:
            if nameID < 1 or nameID > 2:
                raise RuntimeError(f"NameID {nameID} out of bounds")
            else:
                return self.names[nameID - 1]
    
    
    # Maps a Name to its NameID
    async def get_name_id(self, name: str) -> int:
        async with self.lock.reader_lock:
            if name not in self.names:
                raise RuntimeError(f"Name {name} could not be found.")
            else:
                return indexOf(self.names, name)
    
    
    async def check_win(self) -> int:
        async with self.lock.reader_lock:
            num_won_1 = 0
            num_won_2 = 0
            for i in range(NUM_GAMES):
                if self.winners[i] == 0:
                    num_won_1 += 1
                elif self.winners[i] == 1:
                    num_won_2 += 1
            if num_won_1 >= 5 and num_won_2 < 5:
                return 0
            elif num_won_2 >= 5 and num_won_1 < 5:
                return 1
            else:
                return -1
    
    
    # Sets the name of one of the players
    async def set_filename(self: 'BvBScoreboard', filename: str):
        async with self.lock.writer_lock:
            self.filename = filename
    
    
    # Sets the name of one of the players
    async def set_name(self: 'BvBScoreboard', nameID: int, new_name: str):
        async with self.lock.writer_lock:
            if nameID < 1 or nameID > 2:
                raise RuntimeError(f'Index {nameID} for Name is out of bounds')
            self.names[nameID - 1] = new_name
    
    
    # Sets the duo identifier
    async def set_duo(self: 'BvBScoreboard', new_duo: str):
        async with self.lock.writer_lock:
            self.duo = new_duo
    
    
    # Sets a game that can be picked
    async def set_game(self: 'BvBScoreboard', gameID: int, new_game: str):
        async with self.lock.writer_lock:
            if gameID < 1 or gameID > NUM_GAMES:
                raise RuntimeError(f'Index {gameID} for Game is out of bounds')
            id = gameID - 1
            new_game = new_game.lower()
            
            if id in self.picks:
                RuntimeError(f"Could not set game name: This GameID has already been picked")
            elif not await self.manifest.check_game(new_game):
                RuntimeError(f"Could not set game name: {new_game} does not exist in games JSON")
            else:
                self.games[id] = new_game
    
    
    # Toggles the ban status of a game
    async def toggle_ban(self, gameID: int) -> None:
        async with self.lock.writer_lock:
            if gameID < 1 or gameID > NUM_GAMES:
                RuntimeError(f"Cannot toggle ban flag: GameID {gameID} out of bounds")
            else:
                self.bans[gameID - 1] = not self.bans[gameID - 1]
    
    
    # Sets the pick of a game
    async def set_pick(self, pickID: int, gameID: int) -> None:
        async with self.lock.writer_lock:
            if gameID < 1 or gameID > NUM_GAMES:
                raise RuntimeError(f'Index {gameID} for gameID is undefined.')
            elif pickID < 1 or pickID > NUM_PICKS:
                raise RuntimeError(f"Could not set pick: PickID {pickID} is out of bounds.")
            elif self.games[gameID - 1] is None:
                raise RuntimeError(f"Could not set pick: Game at ID {gameID} has not been set.")
            else:
                if self.picks[pickID - 1] == -1:
                    self.num_picked += 1
                self.picks[pickID - 1] = gameID - 1
    
    
    # Sets the chooser of a chosen game
    async def set_chooser(self, pickID: int, nameID: int) -> None:
        async with self.lock.writer_lock:
            if nameID < 1 or nameID > 2:
                raise RuntimeError(f"Could not set pick: NameID {nameID} is out of bounds.")
            elif pickID < 1 or pickID > NUM_PICKS:
                raise RuntimeError(f"Could not set pick: PickID {pickID} is out of bounds.")
            else:
                self.choosers[pickID - 1] = nameID - 1
    
    
    # Sets the winner of a chosen game
    async def set_win(self: 'BvBScoreboard', gameID: int, nameID: int):
        async with self.lock.writer_lock:
            if gameID < 1 or gameID > NUM_GAMES:
                raise RuntimeError(f"GameID {gameID} is out of bounds")
            if nameID < 1 or nameID > 2:
                raise RuntimeError(f"NameID {nameID} is out of bounds")
            
            self.winners[gameID - 1] = nameID - 1
    
    
    async def get_view_embed(self, for_others: bool = False) -> discord.Embed:
        async with self.lock.reader_lock:
            won1 = 0
            won2 = 0
            for i in range(NUM_PICKS):
                p = self.picks[i]
                w = self.winners[p]
                if p != -1:
                    if w == 0:
                        won1 += 1
                    elif w == 1:
                        won2 += 1
            
            duo_name = self.duo
            if duo_name is None:
                duo_name = "Duo is None"
            
            if for_others:
                title = f'\"{duo_name}\"'
            else:
                title = f'\"{duo_name}\" - {self.filename.rpartition("/")[2]}'
            
            embed = discord.Embed(title=title)
            
            nameA = self.names[0]
            if nameA is None:
                nameA = "Name1 is None"
            if won1 > 4:
                nameA = f'**{nameA}**'
            elif won2 > 4:
                nameA = f'~~{nameA}~~'
            gameA_str = "\u200b\n"
            for i in range(6):
                g = self.games[i]
                if g is None:
                    new_game = f'Game{i+1} is None'
                else:
                    new_game = await self.manifest.get_displayname(g)
                if self.bans[i]:
                    gameA_str += f'❌ ~~{new_game}~~\n'
                else:
                    if won1 > 4:
                        if self.winners[i] == 0:
                            gameA_str += f'🟦 **{new_game}**\n'
                        elif self.winners[i] == 1:
                            gameA_str += f'🟥 {new_game}\n'
                        else:
                            gameA_str += f'⬛ ~~{new_game}~~\n'
                    elif won2 > 4:
                        if self.winners[i] == 0:
                            gameA_str += f'🟦 {new_game}\n'
                        elif self.winners[i] == 1:
                            gameA_str += f'🟥 **{new_game}**\n'
                        else:
                            gameA_str += f'⬛ ~~{new_game}~~\n'
                    else:
                        if self.winners[i] == 0:
                            gameA_str += f'🟦 ~~{new_game}~~\n'
                        elif self.winners[i] == 1:
                            gameA_str += f'🟥 ~~{new_game}~~\n'
                        else:
                            gameA_str += f'⬛ **{new_game}**\n'
            if won1 > 4:
                embed.add_field(name=f'🟦 {nameA} 🏆', value=gameA_str, inline=True)
            elif won2 > 4:
                embed.add_field(name=f'🟦 {nameA} 💥', value=gameA_str, inline=True)
            else:
                embed.add_field(name=f'🟦 {nameA}', value=gameA_str, inline=True)
            
            nameN = f'🟪 Neutral'
            gameN_str = "\u200b\n"
            for i in range(6,9):
                g = self.games[i]
                if g is None:
                    new_game = f'Game{i+1} is None'
                else:
                    new_game = await self.manifest.get_displayname(g)
                if self.bans[i]:
                    gameN_str += f'❌ ~~{new_game}~~\n'
                else:
                    if won1 > 4:
                        if self.winners[i] == 0:
                            gameN_str += f'🟦 **{new_game}**\n'
                        elif self.winners[i] == 1:
                            gameN_str += f'🟥 {new_game}\n'
                        else:
                            gameN_str += f'⬛ ~~{new_game}~~\n'
                    elif won2 > 4:
                        if self.winners[i] == 0:
                            gameN_str += f'🟦 {new_game}\n'
                        elif self.winners[i] == 1:
                            gameN_str += f'🟥 **{new_game}**\n'
                        else:
                            gameN_str += f'⬛ ~~{new_game}~~\n'
                    else:
                        if self.winners[i] == 0:
                            gameN_str += f'🟦 ~~{new_game}~~\n'
                        elif self.winners[i] == 1:
                            gameN_str += f'🟥 ~~{new_game}~~\n'
                        else:
                            gameN_str += f'⬛ **{new_game}**\n'
            embed.add_field(name=nameN, value=gameN_str, inline=True)
            
            nameB = self.names[1]
            if nameB is None:
                nameB = "Name2 is None"
            if won1 > 4:
                nameB = f'~~{nameB}~~'
            if won2 > 4:
                nameB = f'**{nameB}**'
            gameB_str = "\u200b\n"
            for i in range(9,15):
                g = self.games[i]
                if g is None:
                    new_game = f'Game{i+1} is None'
                else:
                    new_game = await self.manifest.get_displayname(g)
                if self.bans[i]:
                    gameB_str += f'❌ ~~{new_game}~~\n'
                else:
                    if won1 > 4:
                        if self.winners[i] == 0:
                            gameB_str += f'🟦 **{new_game}**\n'
                        elif self.winners[i] == 1:
                            gameB_str += f'🟥 {new_game}\n'
                        else:
                            gameB_str += f'⬛ ~~{new_game}~~\n'
                    elif won2 > 4:
                        if self.winners[i] == 0:
                            gameB_str += f'🟦 {new_game}\n'
                        elif self.winners[i] == 1:
                            gameB_str += f'🟥 **{new_game}**\n'
                        else:
                            gameB_str += f'⬛ ~~{new_game}~~\n'
                    else:
                        if self.winners[i] == 0:
                            gameB_str += f'🟦 ~~{new_game}~~\n'
                        elif self.winners[i] == 1:
                            gameB_str += f'🟥 ~~{new_game}~~\n'
                        else:
                            gameB_str += f'⬛ **{new_game}**\n'
            if won1 > 4:
                embed.add_field(name=f'🟥 {nameB} 💥', value=gameB_str, inline=True)
            elif won2 > 4:
                embed.add_field(name=f'🟥 {nameB} 🏆', value=gameB_str, inline=True)
            else:
                embed.add_field(name=f'🟥 {nameB}', value=gameB_str, inline=True)
            
            embed.add_field(name='\u200b', value='\u200b', inline=False)
            
            pick_str = "\u200b\n"
            chooser_str = "\u200b\n"
            winner_str = "\u200b\n"
            check1 = 0
            check2 = 0
            for i in range(NUM_PICKS):
                p = self.picks[i]
                c = self.choosers[i]
                if p == -1:
                    if check1 > 4 or check2 > 4:
                        pick_str += f'⬛\n'
                    else:
                        pick_str += f'{number_emojis[i]} Not Picked\n'
                    winner_str += f'⬛\n'
                else:
                    g = self.games[p]
                    w = self.winners[p]
                    dn = await self.manifest.get_displayname(g)
                    if (won1 > 4 and w == 0) or (won2 > 4 and w == 1):
                        if g is None:
                            pick_str += f'{number_emojis[i]} **Game{p + 1}**\n'
                        else:
                            pick_str += f'{number_emojis[i]} **{dn}**\n'
                    elif (won1 > 4 and w == 1) or (won2 > 4 and w == 0):
                        if g is None:
                            pick_str += f'{number_emojis[i]} ~~Game{p + 1}~~\n'
                        else:
                            pick_str += f'{number_emojis[i]} ~~{dn}~~\n'
                    else:
                        if g is None:
                            pick_str += f'{number_emojis[i]} Game{p + 1}\n'
                        else:
                            pick_str += f'{number_emojis[i]} {dn}\n'
                    
                    if w == 0:
                        check1 += 1
                        winner_str += f'🟦 {nameA}\n'
                    elif w == 1:
                        check2 += 1
                        winner_str += f'🟥 {nameB}\n'
                    else:
                        winner_str += f'⬛\n'
                
                if c == 0:
                    chooser_str += f'🟦 {nameA}\n'
                elif c == 1:
                    chooser_str += f'🟥 {nameB}\n'
                else:
                    chooser_str += f'⬛\n'
            
            embed.add_field(name="Picks", value=pick_str, inline=True)
            embed.add_field(name="Choosers", value=chooser_str, inline=True)
            embed.add_field(name="Winners", value=winner_str, inline=True)
            return embed
    
            
    # Runs a set of tests on the values stored in the BvBScoreboard object to ensure
    #   that there is no errors when trying to apply transformations to the object.
    async def test_values(self) -> Literal[True]:
        async with self.lock.reader_lock:
            # Player names must be different
            if None not in self.names:
                if self.names[0] == self.names[1]:
                    raise RuntimeError("Unit test failed - Must have two distinct players")
            
            # 15 games must be assigned before picking
            if self.picks != [-1] * 9:
                for g in range(NUM_GAMES):
                    if self.games[g] is None:
                        raise RuntimeError("Unit test failed - Must finish selecting games before picking")
            
            # Must have done 4-6 bans before picking
            if self.picks != [-1] * 9:
                ban_count = sum(self.bans)
                if ban_count < 4:
                    raise RuntimeError('Unit test failed - Not enough bans made before picks')
                elif ban_count > 6:
                    raise RuntimeError('Unit test failed - An unexpected number of bans have been made')
                    
            # Must pick games in order
            for p in range(1, NUM_PICKS):
                if (self.picks[p] != -1) and (self.picks[p - 1] == -1):
                    raise RuntimeError("Unit test failed - Must pick games in order")
                
            # Game must be picked before a winner can be assigned
            for g in range(NUM_GAMES):
                if (self.winners[g] != -1) and (g not in self.picks):
                    raise RuntimeError("Unit test failed - Game must be picked before a winner can be assigned")
            
            # Game slot must have a pick before assigning a chooser
            for p in range(NUM_PICKS):
                if (self.choosers[p] != -1) and (self.picks[p] == -1):
                    raise RuntimeError("Unit test failed - Game must be picked before a chooser can be assigned")
            
            # Game slot must have a pick and a chooser before assigning a winner
            for g in range(NUM_GAMES):
                if self.winners[g] != -1:
                    if g not in self.picks:
                        raise RuntimeError("Unit test failed - Game must be picked before a winner can be assigned")
                    else:
                        p = indexOf(self.picks, g)
                        if self.choosers[p] == -1:
                            raise RuntimeError("Unit test failed - Game must have a chooser before a winner can be assigned")
            
            # Can't pick a banned game
            for p in range(NUM_PICKS):
                g = self.picks[p]
                if g != -1 and self.bans[g]:
                    raise RuntimeError("Unit test failed - Game cannot be picked if it is banned")
            
            return True
    
    
    # Returns a set of choices from the self.games field with:
    #   Display - ID and Game
    #   Value   - GameID
    async def get_games(self, current: str) -> List[app_commands.Choice[int]]:
        async with self.lock.reader_lock:
            # Get all valid GameIDs in the match
            game_ids = [i for i in range(NUM_GAMES) if self.games[i] is not None]
            
            # For each GameID, up to 25 output objects:
            return_obj = []
            for g in game_ids:
                if len(return_obj) >= 25:
                    break
                display_name = await self.manifest.get_displayname(self.games[g])
                # Collect all GameIDs where the input matches either the ID or the display name
                if current.lower() in f'{g + 1} - {display_name}'.lower():
                    return_obj.append(app_commands.Choice(
                        name=f'{g + 1} - {display_name}',
                        value=g + 1
                    ))
            return return_obj
    
    
    # Returns a set of choices from the self.games field with:
    #   Display - ID and Game
    #   Value   - GameID
    # only returning games that haven't been picked or banned.
    async def get_games_can_pick(self, current: str) -> List[app_commands.Choice[int]]:
        async with self.lock.reader_lock:
            # Get all valid GameIDs that haven't been banned or picked in the match
            game_ids = [i for i in range(NUM_GAMES) if ((self.games[i] is not None) and (not self.bans[i]) and (i not in self.picks))]
            
            # For each GameID, up to 25 output objects:
            return_obj = []
            for g in game_ids:
                if len(return_obj) >= 25:
                    break
                display_name = await self.manifest.get_displayname(self.games[g])
                # Collect all GameIDs where the input matches either the ID or the display name
                if current.lower() in f'{g + 1} - {display_name}'.lower():
                    return_obj.append(app_commands.Choice(
                        name=f'{g + 1} - {display_name}',
                        value=g + 1
                    ))
            return return_obj
    
    
    # Returns a set of choices from the self.picks field with:
    #   Display - PickID and Game
    #   Value   - PickID
    async def get_picks(self, current: str) -> List[app_commands.Choice[int]]:
        async with self.lock.reader_lock:
            pg_ids = [(i, self.picks[i]) for i in range(NUM_PICKS) if (self.picks[i] != -1)]
            
            # For each GameID, up to 25 output objects:
            return_obj = []
            for (p, g) in pg_ids:
                if len(return_obj) >= 25:
                    break
                display_name = await self.manifest.get_displayname(self.games[g])
                # Collect all GameIDs where the input matches either the ID or the display name
                if current.lower() in f'{p + 1} - {display_name}'.lower():
                    return_obj.append(app_commands.Choice(
                        name=f'{p + 1} - {display_name}',
                        value=p + 1
                    ))
            return return_obj
    
    
    # Returns a set of choices from the self.games field with:
    #   Display - PickID and Game
    #   Value   - GameID
    # only returning games that have been picked but not won.
    async def get_picks_not_won(self, current: str) -> List[app_commands.Choice[int]]:
        async with self.lock.reader_lock:
            # Get all valid PickID/GameID pairs for all games that have been picked but not won in the match
            pg_ids = [(i, self.picks[i]) for i in range(NUM_PICKS) if (self.picks[i] != -1) and (self.winners[self.picks[i]] == -1)]
            
            # For each GameID, up to 25 output objects:
            return_obj = []
            for (p, g) in pg_ids:
                if len(return_obj) >= 25:
                    break
                display_name = await self.manifest.get_displayname(self.games[g])
                # Collect all GameIDs where the input matches either the ID or the display name
                if current.lower() in f'{p + 1} - {display_name}'.lower():
                    return_obj.append(app_commands.Choice(
                        name=f'{p + 1} - {display_name}',
                        value=g + 1
                    ))
            return return_obj
    

    # Returns a set of choices from the self.names field with:
    #   Display - Name
    #   Value   - Name
    async def get_names(self, current: str) -> List[app_commands.Choice[int]]:
        async with self.lock.reader_lock:
            names = [(i, self.names[i]) for i in range(2) if self.names[i] is not None]
            return [app_commands.Choice(name=name, value=i+1) for (i, name) in names if current.lower() in name.lower()]