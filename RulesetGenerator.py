from asyncio import Lock, run
from typing import List
from PIL import Image, ImageDraw, ImageFont, ImageOps

BASE_FOLDER = 'scoreboards/base'
RULESET_IMG = 'empty_ruleset.jpeg'

FONT_NAME = ImageFont.truetype(f'{BASE_FOLDER}/Benzin Bold.ttf', 48)
FONT_DUO  = ImageFont.truetype(f'{BASE_FOLDER}/Benzin Bold.ttf', 36)

FONT_GAME_TITLE = ImageFont.truetype(f'{BASE_FOLDER}/BebasNeue-Bold.ttf', 24)
FONT_GAME_TEXT = ImageFont.truetype(f'{BASE_FOLDER}/BebasNeue-Regular.ttf', 18)

FILL_BLUE = (136, 200, 255)
FILL_RED  = (255, 197, 179)
FILL_DUO  = (255, 255, 255)

GAMES_BBOX_X = [86, 515, 997, 1426]
GAMES_BBOX_Y = [524, 687, 850]
GAMES_BBOX_WIDTH  = 413
GAMES_BBOX_HEIGHT = 148
GAMES_OFFSET_OUTSIDE = 19
GAMES_OFFSET_TEXT_X = 13
GAMES_OFFSET_TEXT_Y = 30
GAMES_OFFSET_TOP = 24
GAMES_SIZE = 100

NAMES_BBOX_X         = 86
NAMES_BBOX_Y         = 337
NAMES_BBOX_WIDTH     = 1753
NAMES_BBOX_HEIGHT    = 122
NAMES_OFFSET_OUTSIDE = 130
NAMES_OFFSET_TEXT    = 26
NAMES_OFFSET_TOP     = 18
NAMES_SIZE           = 80
NAMES_OFFSET_CLINE   = 2 + NAMES_BBOX_HEIGHT // 2

class RulesetGenerator:
    lock: Lock
    
    def __init__(self: 'RulesetGenerator'):
        ###
        ### TODO: Need to check that the background image exists
        ###
        temp = Image.open(f'{BASE_FOLDER}/{RULESET_IMG}')
        self.img: Image.Image = Image.new("RGBA", temp.size)
        self.img.paste(temp)
        self.lock = Lock()
    
    async def overlay_game(self: 'RulesetGenerator', game_file: str, game_title: str, index: int, rules: List[str]) -> None:
        ###
        ### TODO: Need to check that the image exists
        ###
        
        async with self.lock:
            # Generate an RGBA image for the picked game.
            temp = Image.open(game_file)
            game_img = Image.new("RGBA", temp.size)
            game_img.paste(temp)
            
            # Scales the pick image to the appropriate height, and crops from the right if not square.
            game_img = ImageOps.scale(game_img, GAMES_SIZE / game_img.height)
            game_img = game_img.crop((0, 0, game_img.height, game_img.height))
            
            # Calculates the location offset at which to place the pick image, based on its index.
            if index < 0 or index > 14 or (index >= 6 and index < 9):
                raise RuntimeError(f"RulesetGenerator.overlay_game({game_file}, {index}) - index is out of bounds")
            elif index < 6:
                game_x = GAMES_BBOX_X[index % 2]
                game_y = GAMES_BBOX_Y[index // 2]
                img_anchor = (game_x + GAMES_OFFSET_OUTSIDE, game_y + GAMES_OFFSET_TOP)
                title_anchor = (game_x + GAMES_OFFSET_OUTSIDE + GAMES_SIZE + GAMES_OFFSET_TEXT_X,
                    game_y + GAMES_OFFSET_TOP)
                text_anchor = (game_x + GAMES_OFFSET_OUTSIDE + GAMES_SIZE + GAMES_OFFSET_TEXT_X,
                    game_y + GAMES_OFFSET_TOP + GAMES_OFFSET_TEXT_Y)
                anchor_position = "la"
                align = 'left'
                title_color = FILL_BLUE
            else:
                game_x = GAMES_BBOX_X[((index - 1) % 2) + 2]
                game_y = GAMES_BBOX_Y[(index - 9) // 2]
                img_anchor = (game_x + GAMES_BBOX_WIDTH - (GAMES_SIZE + GAMES_OFFSET_OUTSIDE),
                    game_y + GAMES_OFFSET_TOP)
                title_anchor = (game_x + GAMES_BBOX_WIDTH - (GAMES_OFFSET_OUTSIDE + GAMES_SIZE + GAMES_OFFSET_TEXT_X),
                    game_y + GAMES_OFFSET_TOP)
                text_anchor = (game_x + GAMES_BBOX_WIDTH - (GAMES_OFFSET_OUTSIDE + GAMES_SIZE + GAMES_OFFSET_TEXT_X),
                    game_y + GAMES_OFFSET_TOP + GAMES_OFFSET_TEXT_Y)
                anchor_position = "ra"
                align = 'right'
                title_color = FILL_RED
            
            # Pastes the final image onto the canvas.
            self.img.paste(game_img, img_anchor, game_img)
            
            draw = ImageDraw.Draw(self.img, "RGBA")
            draw.text(xy=title_anchor, text=game_title, fill=title_color, font=FONT_GAME_TITLE, anchor=anchor_position)
            draw.multiline_text(xy=text_anchor, text="\n".join(rules), fill=FILL_DUO, font=FONT_GAME_TEXT, anchor=anchor_position, align=align)
            

    async def overlay_name(self: 'RulesetGenerator', name_file: str, name: str, index: int):
        ###
        ### TODO: Need to check that the image exists
        ###
        
        async with self.lock:
            # Generate an RGBA image for the picked game.
            temp = Image.open(name_file)
            name_img = Image.new("RGBA", temp.size)
            name_img.paste(temp)
            
            # Scales the image to the appropriate height, and crops from the right if not square.
            name_img = ImageOps.scale(name_img, NAMES_SIZE / name_img.height)
            name_img = name_img.crop((0, 0, name_img.height, name_img.height))
            
            if index == 0:
                # Pastes the image onto the canvas.
                location = (NAMES_BBOX_X + NAMES_OFFSET_OUTSIDE, NAMES_BBOX_Y + NAMES_OFFSET_TOP)
                self.img.paste(name_img, location, name_img)
                
                location = (NAMES_BBOX_X + NAMES_OFFSET_OUTSIDE + NAMES_SIZE + NAMES_OFFSET_TEXT, 
                    NAMES_BBOX_Y + NAMES_OFFSET_CLINE)
                draw = ImageDraw.Draw(self.img, "RGBA")
                draw.text(xy=location, text=name.upper(), fill=FILL_BLUE, font=FONT_NAME, anchor="lm")
                
            elif index == 1:
                # Pastes the image onto the canvas.
                location = (NAMES_BBOX_X + NAMES_BBOX_WIDTH - (NAMES_SIZE + NAMES_OFFSET_OUTSIDE), NAMES_BBOX_Y + NAMES_OFFSET_TOP)
                self.img.paste(name_img, location, name_img)
                
                location = (NAMES_BBOX_X + NAMES_BBOX_WIDTH - (NAMES_SIZE + NAMES_OFFSET_OUTSIDE + NAMES_OFFSET_TEXT), 
                    NAMES_BBOX_Y + NAMES_OFFSET_CLINE)
                draw = ImageDraw.Draw(self.img, "RGBA")
                draw.text(xy=location, text=name.upper(), fill=FILL_RED, font=FONT_NAME, anchor="rm")
            else:
                raise RuntimeError(f"RulesetGenerator.overlay_name({name_file}, {name}, {index}) - index is out of bounds")
    
    async def overlay_duo(self: 'RulesetGenerator', duo: str):
        async with self.lock:
            location = (self.img.width // 2, NAMES_BBOX_Y + NAMES_OFFSET_CLINE)
            draw = ImageDraw.Draw(self.img, "RGBA")
            draw.text(xy=location, text=f'"{duo.upper()}"', fill=FILL_DUO, font=FONT_DUO, anchor="mm")
    
    async def export_img(self: 'RulesetGenerator', img_location: str):
        async with self.lock:
            self.img.save(img_location)

async def main():
    ruleset = RulesetGenerator()
    await ruleset.overlay_duo("Mr Stevens' Twins")
    await ruleset.overlay_name("people/Chris.png", 'Chris', 0)
    await ruleset.overlay_name("people/Joe.png", 'Joe', 1)
    for i in range(15):
        if i >= 6 and i < 9:
            continue
        await ruleset.overlay_game("games/Sims4.png", "The Sims 4", i, ['Test', 'Test', 'Test', 'Test'])
    await ruleset.export_img('test.png')

if __name__ == '__main__':
    run(main())