from aiorwlock import RWLock
from copy import deepcopy
import discord
from discord import app_commands
from GamesManifest import GamesManifest, GAMES_FOLDER
import json
import os
from operator import indexOf
from RulesetGenerator import BASE_FOLDER, RulesetGenerator
from typing import Any, Dict, List, Literal, Optional
from zbdsh_lib import unpack_tuple

NUM_GAMES = 15
NUM_PICKS = 9

DEFAULT_IMG = f'{BASE_FOLDER}/placeholder.png'
PEOPLE_FOLDER = 'people'

# JSON format for scoring
# Duo:             str   - Name of the duo
# Names:     List [str]  - Names of the two players
# Games:     List [str]  - Names of the 15 games
# Rules: List[List[str]] - Set of rules for the 15 games

class BvBRuleset:
    lock: RWLock
    generator: RulesetGenerator
    
    # Initialises the BvBRuleset object with a base set of properties
    def __init__(self: 'BvBRuleset', manifest: GamesManifest):
        self.duo:       Optional[str]        =   None                          # Name of the duo
        self.names:     List[Optional[str]]  =  [None] * 2                     # Names of the two players
        self.games:     List[str]            =  [None] * NUM_GAMES             # Names of the 15 games
        self.rules:     List[List[str]]      =  []
        for i in range(NUM_GAMES):
            self.rules.append(['','','','','']) # Rules for the 15 games
        self.filename = ''
        self.manifest = manifest
        self.lock = RWLock()
        self.generator = RulesetGenerator()
    
    # Imports the values from the stored JSON object that this object can export
    async def import_json(self, json_location: str) -> None:
        async with self.lock.writer_lock:
            if os.path.exists(json_location):
                with open(json_location, 'r') as f:
                    json_dict = json.load(f)
            else:
                raise RuntimeError(f'import_json({json_location}) - Could not find file at location specified')
            
            self.duo = json_dict['Duo']
            self.names =    deepcopy(unpack_tuple(json_dict['Names']))
            self.games =    deepcopy(unpack_tuple(json_dict['Games']))
            self.rules =    deepcopy(unpack_tuple(json_dict['Rules']))
            self.filename = json_location

    
    # Exports the values in the BvBScoreboard object to a JSON object that can be later imported
    async def export_json(self, json_location: str = None) -> None:
        async with self.lock.reader_lock:
            output_json : Dict[str, Any] = {}
            
            output_json['Duo'] =    self.duo
            output_json['Names'] =  deepcopy(self.names)
            output_json['Games'] =  deepcopy(self.games)
            output_json['Rules'] =  deepcopy(self.rules)
            
            if json_location is None:
                json_location = self.filename
            
            with open(json_location, 'w') as f:
                json.dump(output_json, f, indent=1, separators=(', ', ': '))
            
            os.chmod(json_location, 0o774)
    
    
    async def export_img(self, output_location: str = None) -> None:
        async with self.lock.reader_lock:
            if output_location is None:
                output_location = f"{self.filename.removesuffix('.json')}.png"
            
            if self.duo is not None:
                await self.generator.overlay_duo(self.duo)
                
            for i in range(2):
                if self.names[i] is not None:
                    await self.generator.overlay_name(f'{PEOPLE_FOLDER}/{self.names[i]}.png', self.names[i], i)
                else:
                    await self.generator.overlay_name(DEFAULT_IMG, '', i)
            
            for i in range(NUM_GAMES):
                # Skip neutral rules
                if i >= 6 and i < 9:
                    continue
                # If the game at the ID exists, get its filename, otherwise use the default.
                if await self.manifest.check_game(self.games[i]):
                    filename = f'{GAMES_FOLDER}/{await self.manifest.get_filename(self.games[i])}'
                    gamename = await self.manifest.get_displayname(self.games[i])
                else:
                    filename = DEFAULT_IMG
                    gamename = "Unknown"
                
                # Overlay the pick on the base image.
                await self.generator.overlay_game(filename, gamename, i, self.rules[i][1:])
            
            await self.generator.export_img(output_location)
    
    
    async def get_filename(self) -> str:
        async with self.lock.reader_lock:
            return self.filename
    
    
    # Maps a GameID to its Game in the self.games field
    async def get_game(self, gameID: int):
        async with self.lock.reader_lock:
            if gameID < 1 or gameID > NUM_GAMES:
                raise RuntimeError(f"GameID {gameID} out of bounds")
            else:
                return self.games[gameID - 1]
    
    
    # Maps a Game to its first appearing GameID in the self.games field
    async def get_game_id(self, game: str) -> int:
        async with self.lock.reader_lock:
            if game not in self.games:
                raise RuntimeError(f"Game {game} could not be found.")
            else:
                return indexOf(self.games, game)
    
    
    # Maps a GameID to its display name
    async def get_game_display(self, gameID: int) -> str:
        async with self.lock.reader_lock:
            if gameID < 1 or gameID > NUM_GAMES:
                raise RuntimeError(f"GameID {gameID} out of bounds")
            else:
                return await self.manifest.get_displayname(self.games[gameID - 1])
    
    
    # Maps a NameID to its Name
    async def get_name(self, nameID: str) -> str:
        async with self.lock.reader_lock:
            if nameID < 1 or nameID > 2:
                raise RuntimeError(f"NameID {nameID} out of bounds")
            else:
                return self.names[nameID - 1]
    
    
    # Maps a Name to its NameID
    async def get_name_id(self, name: str) -> int:
        async with self.lock.reader_lock:
            if name not in self.names:
                raise RuntimeError(f"Name {name} could not be found.")
            else:
                return indexOf(self.names, name)
    
    
    # Sets the name of one of the players
    async def set_filename(self, filename: str):
        async with self.lock.writer_lock:
            self.filename = filename
    
    
    # Sets the name of one of the players
    async def set_name(self, nameID: int, new_name: str):
        async with self.lock.writer_lock:
            if nameID < 1 or nameID > 2:
                raise RuntimeError(f'Index {nameID} for Name is out of bounds')
            self.names[nameID - 1] = new_name
    
    
    # Sets the duo identifier
    async def set_duo(self, new_duo: str):
        async with self.lock.writer_lock:
            self.duo = new_duo
    
    
    # Sets a game that can be picked
    async def set_game(self, gameID: int, new_game: str):
        async with self.lock.writer_lock:
            if gameID < 1 or gameID > NUM_GAMES:
                raise RuntimeError(f'Index {gameID} for Game is out of bounds')
            id = gameID - 1
            new_game = new_game.lower()
            
            if not await self.manifest.check_game(new_game):
                RuntimeError(f"Could not set game name: {new_game} does not exist in games JSON")
            else:
                self.games[id] = new_game
                self.rules[id][0] = new_game
    
    
    # Returns a set of choices from the self.games field with:
    #   Display - ID and Game
    #   Value   - GameID
    async def get_games(self, current: str) -> List[app_commands.Choice[int]]:
        async with self.lock.reader_lock:
            # Get all valid GameIDs in the match
            game_ids = [i for i in range(NUM_GAMES) if self.games[i] is not None]
            
            # For each GameID, up to 25 output objects:
            return_obj = []
            for g in game_ids:
                if len(return_obj) >= 25:
                    break
                display_name = await self.manifest.get_displayname(self.games[g])
                # Collect all GameIDs where the input matches either the ID or the display name
                if current.lower() in f'{g + 1} - {display_name}'.lower():
                    return_obj.append(app_commands.Choice(
                        name=f'{g + 1} - {display_name}',
                        value=g + 1
                    ))
            return return_obj
    
    
    # Returns a set of choices from the self.games field with:
    #   Display - ID and Game
    #   Value   - GameID
    # only returning games that haven't been picked or banned.
    async def get_games_can_pick(self, current: str) -> List[app_commands.Choice[int]]:
        async with self.lock.reader_lock:
            # Get all valid GameIDs that haven't been banned or picked in the match
            game_ids = [i for i in range(NUM_GAMES) if ((self.games[i] is not None) and (not self.bans[i]) and (i not in self.picks))]
            
            # For each GameID, up to 25 output objects:
            return_obj = []
            for g in game_ids:
                if len(return_obj) >= 25:
                    break
                display_name = await self.manifest.get_displayname(self.games[g])
                # Collect all GameIDs where the input matches either the ID or the display name
                if current.lower() in f'{g + 1} - {display_name}'.lower():
                    return_obj.append(app_commands.Choice(
                        name=f'{g + 1} - {display_name}',
                        value=g + 1
                    ))
            return return_obj
    
    
    # Returns a set of choices from the self.picks field with:
    #   Display - PickID and Game
    #   Value   - PickID
    async def get_picks(self, current: str) -> List[app_commands.Choice[int]]:
        async with self.lock.reader_lock:
            pg_ids = [(i, self.picks[i]) for i in range(NUM_PICKS) if (self.picks[i] != -1)]
            
            # For each GameID, up to 25 output objects:
            return_obj = []
            for (p, g) in pg_ids:
                if len(return_obj) >= 25:
                    break
                display_name = await self.manifest.get_displayname(self.games[g])
                # Collect all GameIDs where the input matches either the ID or the display name
                if current.lower() in f'{p + 1} - {display_name}'.lower():
                    return_obj.append(app_commands.Choice(
                        name=f'{p + 1} - {display_name}',
                        value=p + 1
                    ))
            return return_obj
    
    
    # Returns a set of choices from the self.games field with:
    #   Display - PickID and Game
    #   Value   - GameID
    # only returning games that have been picked but not won.
    async def get_picks_not_won(self, current: str) -> List[app_commands.Choice[int]]:
        async with self.lock.reader_lock:
            # Get all valid PickID/GameID pairs for all games that have been picked but not won in the match
            pg_ids = [(i, self.picks[i]) for i in range(NUM_PICKS) if (self.picks[i] != -1) and (self.winners[self.picks[i]] == -1)]
            
            # For each GameID, up to 25 output objects:
            return_obj = []
            for (p, g) in pg_ids:
                if len(return_obj) >= 25:
                    break
                display_name = await self.manifest.get_displayname(self.games[g])
                # Collect all GameIDs where the input matches either the ID or the display name
                if current.lower() in f'{p + 1} - {display_name}'.lower():
                    return_obj.append(app_commands.Choice(
                        name=f'{p + 1} - {display_name}',
                        value=g + 1
                    ))
            return return_obj
    

    # Returns a set of choices from the self.names field with:
    #   Display - Name
    #   Value   - Name
    async def get_names(self, current: str) -> List[app_commands.Choice[int]]:
        async with self.lock.reader_lock:
            names = [(i, self.names[i]) for i in range(2) if self.names[i] is not None]
            return [app_commands.Choice(name=name, value=i+1) for (i, name) in names if current.lower() in name.lower()]