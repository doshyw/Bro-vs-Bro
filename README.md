# Bro vs Bro Discord Bot

This repo allows for the deployment of a Discord Bot that controls the automated creation of scorecards for a personal competition that I am having with some friends (the initial concept was taken from a YouTuber, [Ludwig](https://www.youtube.com/c/Ludwigahgren)).

The general concepts for each match in the competition are as such:
- A set of people compete in 1v1 matches.
- Each player picks 6 different games, with their own choice of rulesets, to play against each other. This is also padded with 3 neutral games, to use as a tiebreaker.
- After each person has selected their set of games, they are revealed to each other, and they each have the opportunity to ban two games from each others' set. This is then followed by each player banning on neutral game.
- After the bans, the players take it in turns to select games from the full set (excepting the neutral game) to compete in, one at a time. A coinflip decides the first picker, and the loser of each game picks the next.
- The winner of the match is the first player to 5 game wins, with a 4-4 tiebreaker being decided with the use of the neutral game.

From a high level perspective, the bot allows the following commands to setup a scorecard:
- `/score init <name1> <name2>` allows the creation of a new scorecard.
- `/score load <filename>` allows a scorecard file to be created, `/score save` allows the open file to be saved, and `/score close` allows the open file to be closed.
- `/score view` creates a Unicode embed that represents the contents of the scorecard file and sends it to the current Discord channel.
- `/score set game <index> <game>` allows each of the 15 available game slots to be assigned a game. Each player gets 6 "picks", and there are 3 neutral games.
- `/score ban <game>` bans a particular game choice on the scorecard.
- `/score pick <game> <name>` records that a particular player has picked a particular game as the next to play.
- `/score win <game> <name>` records that a particular player has won a particular picked game.
- `/score publish` creates the scorecard as a high-res PNG and sends it to a particular Discord channel.

Please note that certain elements of its functionality are specifically configured for use on my personal devices (mainly the `.env` file), and so this may need some finetuning in order to run elsewhere. It mainly serves to evidence the work done to complete the project.

See the images under `examples/` for the base scorecard and a completed example.
